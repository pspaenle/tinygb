#!/bin/bash

TINYGB_DIR=$(pwd)

cd external_libs
#wget -nc https://forge.imag.fr/frs/download.php/592/givaro-3.8.0.tar.gz
#wget -nc http://linalg.org/fflas-ffpack-2.0.0.tar.gz
#wget -nc http://github.com/xianyi/OpenBLAS/archive/v0.2.14.tar.gz
tar xzf givaro-4.0.2.tar.gz
tar xzf fflas-ffpack-2.2.2.tar.gz
tar xzf OpenBLAS-0.2.20.tar.gz
cd givaro-4.0.2
./configure --prefix $TINYGB_DIR
cd ..
cd ..
mkdir include
mkdir lib

export OPENBLAS_NUM_THREADS=1
export GOTO_NUM_THREADS=1
export OMP_NUM_THREADS=1

cd external_libs/OpenBLAS-0.2.20
make
make install PREFIX=$TINYGB_DIR
cd $TINYGB_DIR

cd external_libs/givaro-4.0.2 && make && make install
cd $TINYGB_DIR

cd external_libs/fflas-ffpack-2.2.2
./configure GIVARO_LIBS="$TINYGB_DIR/lib/libgivaro.a -lgmp -lgmpxx" \
        GIVARO_CFLAGS=-I$TINYGB_DIR/include \
       	--with-blas-libs="$TINYGB_DIR/lib/libopenblas.a -lpthread" \
	--with-blas-cflags=-I$TINYGB_DIR/include \
	--enable-optimization --prefix=$TINYGB_DIR
make
make install
