Important warning: direct compilation works only for HASWELL architectures. For
other architectures, change the target value for the compilation of OpenBlas.

tinygb v1.1
-----------

tinygb is a Gröbner basis engine that relies on the MPFQ library and
partially on FFLAS-FFPACK and OpenBLAS for linear algebra computations.
The installation requires the software construction tool scons
(https://scons.org/)

I - Installation
-----------------

  ./build_external_libs.sh
  ./compile.sh


II - Checking the installation
------------------------------

Running the command

  ./check.sh

in the build directory verifies that the program has been correctly compiled.

III - Usage
-----------

1) Input format

tinygb takes as input a polynomial system in the following format:

- the first line is the number of variables
- the second line is the cardinality of the field
- the next lines contains the list of terms of the polynomials, each
  line is a element of the finite field followed by the vector of
  exponents of the corresponding monomial. Polynomials are ended
  by a line containing a semicolon. Examples are provided in the
  directory "tests".


IV - Libraries included in the distribution
-------------------------------------------

tinygb is distributed with the sources of the libraries FFLAS-FFPACK-2.0.0
(under licence LGPL-2.1++), Givaro-3.8 (under licence CeCILL-B V1), and
OpenBLAS-0.2.14 (under BSD licence, modified).  The only modification of
OpenBLAS-0.2.14 is a line on top of the Makefile to ensure build a monothreaded
version of the library. The sources of external libraries are located in the
directory external_libs of tinygb.

