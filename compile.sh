#!/bin/bash

TINYGB_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $TINYGB_PATH

if [ ! -f param.sh ]; then 
  cp param.template.sh param.sh
fi

source param.sh

if [ "$TINYGB_DEBUG" -eq 1 ]; then
  scons debug=1 1>&2
else
  scons 1>&2
fi

exit $?
