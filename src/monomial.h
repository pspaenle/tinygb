
#ifndef __MONOMIAL_H__
#define __MONOMIAL_H__

#include <assert.h>
#include <cstdint>
#include <iostream>
#include <malloc.h>
#include <utility>
#include <vector>
#include "./common.h"
#include "./glob_params.h"
#include "finite_field_arithmetic/givaro_wrapper.h"

namespace tinygb {

typedef std::uint32_t expo_int;
//typedef expo_int monomial_carrier[NB_VARS];


// TODO : iterators are invalidated when memory_pool is resized
class Monomial_pool {
  public:
  std::vector<expo_int> memory_pool;
  std::vector<std::size_t> free_places;

  std::size_t monomial_alloc() {
    if (!free_places.empty())
    {
      std::size_t res = free_places.back();
      free_places.pop_back();
      return res;
    } else {
      std::size_t mpool_size = memory_pool.size();
      memory_pool.resize(mpool_size*2 + NB_VARS, 0);
      for (std::size_t i = mpool_size + NB_VARS; 
           i < memory_pool.size(); 
           i += NB_VARS)
        free_places.push_back(i);
      return mpool_size;
    }
  }

  void monomial_free(std::size_t i) {
    free_places.push_back(i);
    //TODO: perhaps defragment if too much memory is allocated
  }

  expo_int& operator[](std::size_t i) { return memory_pool[i]; }
};


class Monomial {
  std::size_t pool_pos;
  static Monomial_pool m_pool;

 public:

  /* Constructors and destructor
   ***************************************************************************/

  explicit Monomial(const expo_int *_exp) {
    pool_pos = m_pool.monomial_alloc();
    for (unsigned i = 0; i < NB_VARS; ++i)
      m_pool[pool_pos + i] = _exp[i];
  }

  Monomial(const Monomial& m2) {
    pool_pos = m_pool.monomial_alloc();
    for (unsigned i = 0; i < NB_VARS; ++i)
      m_pool[pool_pos + i] = m_pool[m2.pool_pos + i];
  }

  Monomial() {
    pool_pos = m_pool.monomial_alloc();
    for (unsigned i = 0; i < NB_VARS; ++i)
      m_pool[pool_pos + i] = 0;
  }

  ~Monomial() {
    m_pool.monomial_free(pool_pos);
  }

  /* Getter and setter
   ***************************************************************************/

  expo_int getexp(unsigned i) const { return m_pool[pool_pos + i]; }
  void setexp(unsigned i, expo_int v) { m_pool[pool_pos + i] = v; }
  expo_int& operator() (unsigned i) { return m_pool[pool_pos + i]; }

  /* Assignment
   ***************************************************************************/

  Monomial& operator=(const Monomial& m2) {
    for (unsigned i = 0; i < NB_VARS; ++i)
      setexp(i, m2.getexp(i));
    return *this;
  }

  /* Arithmetic operations
   ***************************************************************************/

  inline expo_int Degree() const {
    expo_int R = 0;
    for (unsigned i = 0; i < NB_VARS; ++i)
      R += getexp(i);
    return R;
  }

  Monomial operator*(const Monomial &m2) const {
    Monomial R;
    for (unsigned i = 0; i < NB_VARS; ++i)
      R.setexp(i, getexp(i) + m2.getexp(i));
    return R;
  }

  void operator*=(const Monomial &m2) {
    for (unsigned i = 0; i < NB_VARS; ++i)
      setexp(i, getexp(i) + m2.getexp(i));
  }

  Monomial operator/(const Monomial &m2) const {
    assert(IsDivisibleBy(m2));
    Monomial R;
    for (unsigned i = 0; i < NB_VARS; ++i)
      R.setexp(i, getexp(i) - m2.getexp(i));
    return R;
  }

  bool operator<(const Monomial&) const;

  bool operator!=(const Monomial &m2) const {
    for (unsigned i = 0; i < NB_VARS; ++i)
      if (getexp(i) != m2.getexp(i))
        return true;
    return false;
  }

  bool operator==(const Monomial &m2) const {
    for (unsigned i = 0; i < NB_VARS; ++i)
      if (getexp(i) != m2.getexp(i))
        return false;
    return true;
  }

  static Monomial LCM(const Monomial&, const Monomial&);
  std::pair<Monomial, Monomial> Cofactors(const Monomial&) const;
  bool IsDivisibleBy(const Monomial&) const;


  unsigned Valuation(const std::vector<unsigned>& w) const {
    unsigned result = 0;
    for (std::size_t i = 0; i < NB_VARS; ++i)
      result += w[i]*getexp(i);
    return result;
  }

  /* I/O
   ***************************************************************************/

  std::ostream& Print(std::ostream&) const;
  friend std::ostream& operator<<(std::ostream&, const Monomial&);
};


// ordering on terms
template <class T>
static inline bool operator<(const std::pair<Monomial, T>& p1,
    const std::pair<Monomial, T>& p2) {
  return p1.first < p2.first;
}

}  // namespace tinygb

#endif  // MONOMIAL_H_
