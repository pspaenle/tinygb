/* glob_param.h
 ******************************************************************************
 This file contains a class for global environment variables which should not
 change at any point during the computation.
 At the beginning of the computation, all these parameters should be saved in 
 a file in the current work directory, and they should be loaded from this file
 by subprograms.
 The only global parameter which should be handled at the global level is the
 name of the work directory.

 Warning: This file contains definitions of variables.
 *****************************************************************************/

#ifndef __GLOB_PARAMS_H__
#define __GLOB_PARAMS_H__

#include <string>
#include <cstdint>
#include <iostream>
#include <fstream>

namespace tinygb {

/* Global variables
 *****************************************************************************/

// The full path to the work directory, finishing with '/'. ___________________
extern std::string WORKDIR;

// The number of variables in the polynomial system. __________________________
extern std::size_t NB_VARS;

// The cardinality of the finite field. _______________________________________
extern std::uint64_t FIELD_CARD;

// A string encoding the monomial ordering. ___________________________________
extern std::string MONOMIAL_ORDERING;

// These global variables are defined if they are included in an executable.

#ifdef TINYGB_EXECUTABLE
std::string WORKDIR;
std::size_t NB_VARS;
std::uint64_t FIELD_CARD;
std::string MONOMIAL_ORDERING;
#endif

/* Save/load functions
 *****************************************************************************/

void save_glob_params(const std::string& filename);
void load_glob_params(const std::string& filename);

}  // namespace tinygb

#endif
