#ifndef __F4_LAUNCHER_H__
#define __F4_LAUNCHER_H__

#include <string>
#include <vector>
#include "./common.h"
#include "./f4_toplevel.h"
#include "./polynomial.h"
#include "./parser.h"
#include "./utils.h"

namespace tinygb {


// What are these declarations for?
template <class T>
void field_init();

template <class T>
void field_clear();

template <class T>
void launchF4(std::ostream &out) {
//  log(LOG_INFO) << "begin parsing" << std::endl;
//  Parser parser(in);
//  std::vector<Polynomial> sys = parser.parse();
//  log(LOG_INFO) << "parsing done" << std::endl;
  std::vector<Polynomial> gb = F4();
  for (const auto& f : gb) {
    f.Print(out);
    out << std::endl;
  }
}

void initF4(std::string wdir, const std::string& sys_file, std::ostream &out) {
//  log(LOG_INFO) << "Initialization" << std::endl;
//  in >> NB_VARS;
//  in >> FIELD_CARD;
//  MONOMIAL_ORDERING = "grevlex";
//  try {
//    save_glob_params(WORKDIR + "/global_parameters.txt");
//  } catch (const std::exception& e) {
//    std::cout << "f4launcher: " << e.what() << std::endl;
//  }
  WORKDIR = wdir;

  std::system(("mkdir -p " + WORKDIR + "/matrices").c_str());
  std::system(("build/tinygb_init/tinygb_init " + WORKDIR + " " + sys_file).c_str());
 
  TINYGB_TRY(tinygb::load_glob_params(tinygb::WORKDIR + "/global_parameters.txt"), "f4launcher");

  GFp_Givaro::k = GFp_Givaro::ring_t(Givaro::Integer(FIELD_CARD));
  std::cout << NB_VARS    << std::endl;
  std::cout << FIELD_CARD << std::endl;

  launchF4<GivaroWrapper>(out);
}

}  // namespace tinygb
#endif  // F4_LAUNCHER_H_
