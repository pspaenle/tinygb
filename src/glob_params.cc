#include "./glob_params.h"
#include "finite_field_arithmetic/givaro_wrapper.h"

namespace tinygb {

void save_glob_params(const std::string& filename)
{
  std::ofstream file_params(
      filename, 
      std::ios::in | std::ios::trunc);
  if (!file_params.is_open()) 
    throw std::runtime_error("Unable to open file " + filename);

  file_params << NB_VARS           << std::endl;
  file_params << FIELD_CARD        << std::endl;
  file_params << MONOMIAL_ORDERING << std::endl;
  file_params.close();
}

void load_glob_params(const std::string& filename) {
  std::ifstream file_params(
      filename,
      std::ios::out);
  if (!file_params.is_open()) 
    throw std::runtime_error("Unable to open file " + filename);

  file_params >> NB_VARS;
  file_params >> FIELD_CARD;
  file_params >> MONOMIAL_ORDERING;
  GFp_Givaro::k = GFp_Givaro::ring_t(Givaro::Integer(FIELD_CARD));
  
  file_params.close();
}

}  // namespace tinygb
