#ifndef __F4_TOPLEVEL_H__
#define __F4_TOPLEVEL_H__

// Options. 
// TODO: move to a better place
namespace tinygb {
static bool MAXSTEP_FLAG = false;
static unsigned int MAXSTEP = 0;
static bool STOP_CRITERION = false;
}

#include <algorithm>
#include <list>
#include <map>
#include <set>
#include <utility>
#include <vector>
#include "./common.h"
#include "./critical_pair.h"
#include "linalg/matrix_factory.h"
#include "linalg/storage_matrix.h"
#include "./polynomial.h"
#include "./utils.h"
#include "./macros.h"
#include "./logger.h"

namespace tinygb {

// Toplevel F4.
std::vector<Polynomial> F4() {
  std::vector<PolynomialInMatrix> gb;
  std::vector<CriticalPair>       global_critical_pairs;
  std::string input_sys{"interreduced_system"};

  std::map<std::string, std::shared_ptr<StorageMatrix>> UniqueMatIds;
  std::vector<std::shared_ptr<const StorageMatrix>> list_F4_matrices;

  std::ifstream insys;
  //TODO: test if file is open, else throw exception

  std::system(("build/addcrit/addcrit " + WORKDIR + " " + input_sys
               + " critpairs_glob.txt gb.txt").c_str());

  TINYGB_TRY(LoadCritPairs("critpairs_glob.txt", global_critical_pairs, UniqueMatIds), "f4");
  TINYGB_TRY(LoadGB("gb.txt", gb, UniqueMatIds), "f4");

  unsigned step_number = 1;

  std::set<Monomial> GB_LeadingMonomials;
  for (const auto& g : gb)
    GB_LeadingMonomials.insert(g.LeadingMonomial());
  bool stop_crit = HasAllPowersOfVars(GB_LeadingMonomials);
  
  std::string list_id_reductors = input_sys + " ";

  while (!global_critical_pairs.empty() && 
      (!MAXSTEP_FLAG || step_number < MAXSTEP) && 
      (!STOP_CRITERION || !stop_crit)) {

    std::cerr << "STEP " << step_number << std::endl;
    Logger("/tmp/log_tinygb").log("--- STEP " + std::to_string(step_number) + " ---");
    TINYGB_TRY(SaveCritPairs("critpairs_glob.txt", global_critical_pairs), "f4");

    std::system(("build/selcrit/selcrit " + WORKDIR + " critpairs_glob.txt critpairs_step.txt").c_str());
    std::system(("build/selred/selred " + WORKDIR + " critpairs_step.txt reductors_step.txt "
          + list_id_reductors).c_str());
    std::system(("build/buildmats/buildmats " + WORKDIR + " critpairs_step.txt reductors_step.txt " + 
          "matA matB matC matD").c_str());
    std::system(("build/utsolve/utsolve " + WORKDIR + " matB2 matA matB").c_str());
    std::system(("build/matmulsub/matmulsub " + WORKDIR + " matR matB2 matC matD").c_str());
    std::system(("build/rowech/rowech " + WORKDIR + " matD2 matB3 matR matB2").c_str());

    std::shared_ptr<RowMajMat> B, D;

    TINYGB_TRY(ReadRowMajMat(&D, "matD2"), "f4");
    TINYGB_TRY(ReadRowMajMat(&B, "matB3"), "f4");

    auto p_current_storage_matrix1 = std::make_shared<const StorageMatrix>(B, B->col_dec_, B->row_dec_);
    p_current_storage_matrix1->Store("matrices/" + p_current_storage_matrix1->id() + ".mat");

    auto p_current_storage_matrix2 = std::make_shared<const StorageMatrix>(D, D->col_dec_, D->row_dec_);
    p_current_storage_matrix2->Store("matrices/" + p_current_storage_matrix2->id() + ".mat");

    list_id_reductors += p_current_storage_matrix1->id() + " ";
    list_id_reductors += p_current_storage_matrix2->id() + " ";

    TINYGB_TRY(SaveGB("gb.txt", gb), "f4");

    std::system(("build/addcrit/addcrit " + WORKDIR + " " + p_current_storage_matrix2->id()
                 + " critpairs_glob.txt gb.txt").c_str());

    TINYGB_TRY(LoadGB("gb.txt", gb, UniqueMatIds), "f4");
    TINYGB_TRY(LoadCritPairs("critpairs_glob.txt", global_critical_pairs, UniqueMatIds), "f4");

    step_number++;
    GB_LeadingMonomials.clear();
    for (const auto& g : gb)
      GB_LeadingMonomials.insert(g.LeadingMonomial());
    stop_crit = HasAllPowersOfVars(GB_LeadingMonomials);
  }
  std::vector<Polynomial> final_gb;
  for (const auto& f : gb)
    final_gb.push_back(f.to_polynomial());

  std::sort(final_gb.begin(), final_gb.end());
  std::reverse(final_gb.begin(), final_gb.end());
  return final_gb;
}

}  // namespace tinygb
#endif  // F4_TOPLEVEL_H_
