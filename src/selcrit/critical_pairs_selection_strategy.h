#ifndef __CRITICAL_PAIRS_SELECTION_STRATEGY_H__
#define __CRITICAL_PAIRS_SELECTION_STRATEGY_H__
#include "./critical_pair.h"

// This file contains strategies for selecting critical pairs. All classes in
// this file must implement the static method SelectCriticalPairs.

namespace tinygb {
  
// TODO(pj): at this point, works only for homogeneous systems or graded
// orderings.
class NormalStrategy {
  public:
// Selects critical pairs to reduce during a step from the current list of
// critical pairs. Removes the selected pairs from the global list.
  static std::vector<CriticalPair> SelectCriticalPairs(
      std::vector<CriticalPair> &global_critical_pairs) {

    unsigned step_degree;
    std::vector<CriticalPair> step_critical_pairs;

    step_degree = global_critical_pairs.front().Degree();

    for (const auto& current_critical_pair : global_critical_pairs)
      if (current_critical_pair.Degree() < step_degree)
        step_degree = current_critical_pair.Degree();

    for (auto it = global_critical_pairs.begin(); 
        it != global_critical_pairs.end(); ++it) {
      if (it->Degree() == step_degree) {
        step_critical_pairs.push_back(*it);
        it = global_critical_pairs.erase(it);
        it--;
      }
    }

    return step_critical_pairs;
  }
};

//  class SparseStrategy {
//    public:
//  // Selects Pairs using the following criterion : the ones minimizing the max of
//  // the linear forms on the exponent vectors.
//  // TODO(pj): write more details for the doc.
//    static std::vector<std::vector<unsigned> > linear_forms;
//  
//  // Selects critical pairs to reduce during a step from the current list of
//  // critical pairs. Removes the selected pairs from the global list.
//  // TODO(pj): beware of code duplication!
//    static std::vector<CriticalPair> SelectCriticalPairs(
//        std::vector<CriticalPair> &global_critical_pairs) {
//    unsigned step_degree;
//    std::vector<CriticalPair> step_critical_pairs;
//  
//    step_degree = SparseDegree(global_critical_pairs.front());
//    for (const auto& current_critical_pair : global_critical_pairs)
//      if (current_critical_pair.Degree() < step_degree)
//        step_degree = SparseDegree(current_critical_pair);
//  
//    for (auto it = global_critical_pairs.begin(); 
//         it != global_critical_pairs.end(); 
//         ++it)
//      if (SparseDegree(*it) == step_degree) {
//        step_critical_pairs.push_back(*it);
//        it = global_critical_pairs.erase(it);
//        it--;
//      }
//    return step_critical_pairs;
//    }
//  
//   private:
//    static unsigned SparseDegree(const CriticalPair&);
//  };
//  
//  std::vector<std::vector<unsigned> > SparseStrategy::linear_forms;
//  
//  unsigned SparseStrategy::SparseDegree(const CriticalPair& crit_pair) {
//    std::list<Monomial> list_monomials = crit_pair.GetMonomials();
//    unsigned sparse_deg = 0;
//    for (const auto& m : list_monomials)
//      for (const auto& w : linear_forms) {
//        unsigned tmp_deg = m.Valuation(w);
//        if (tmp_deg > sparse_deg)
//          sparse_deg = tmp_deg;
//      }
//    return sparse_deg;
//  }

}  // namespace tinygb

#endif  // CRITICAL_PAIRS_SELECTION_STRATEGY_H_
