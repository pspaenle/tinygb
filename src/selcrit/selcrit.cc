#define TINYGB_EXECUTABLE

#include "./glob_params.h"
#include "./matrices/rowmajmat.h"
#include "./finite_field_arithmetic/givaro_wrapper.h"
#include "./common.h"
#include "./utils.h"
#include "./critical_pairs_selection_strategy.h"
#include "./macros.h"
#include "./logger.h"

namespace tinygb {

void selcrit(const std::string& infile,
             const std::string& outfile) {
  std::vector<CriticalPair> VecCrit;
  std::map<std::string, std::shared_ptr<StorageMatrix>> UniqueMatIds;
  TINYGB_TRY(LoadCritPairs(infile, VecCrit, UniqueMatIds), "selcrit");

  std::vector<CriticalPair> Select = 
    NormalStrategy::SelectCriticalPairs(VecCrit);
  
  std::cerr << "selcrit:" << std::endl;
  std::cerr << "  nb pairs: " << Select.size() << std::endl;
  TINYGB_TRY(SaveCritPairs(infile, VecCrit), "selcrit");
  TINYGB_TRY(SaveCritPairs(outfile, Select), "selcrit");
}

}  // namespace tinygb

int main(int argc, char **argv) {
  tinygb::Logger("/tmp/log_tinygb").log("selcrit   start");

  (void)argc; // To get rid of the warning -Wunused-parameter argc

  tinygb::WORKDIR = (std::string)argv[1];

  TINYGB_TRY(tinygb::load_glob_params(tinygb::WORKDIR + "/global_parameters.txt"), "selcrit");

  tinygb::selcrit(argv[2], argv[3]);
  tinygb::Logger("/tmp/log_tinygb").log("selcrit   stop");
  return EXIT_SUCCESS;
}

