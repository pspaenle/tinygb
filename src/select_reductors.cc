#include "./select_reductors.h"

namespace tinygb {

std::vector<std::pair<Monomial, PolynomialInMatrix> > SelectReductors(
    const std::vector<CriticalPair>& step_critical_pairs,
    const std::vector<PolynomialInMatrix>& global_reductors) {
  // Vector of selected reductors for this step.
  std::vector<std::pair<Monomial, PolynomialInMatrix> > step_reductors;
  // Current reductor to be inserted in step_reductors.
  PolynomialInMatrix reductor;
  // Pointer to the best reductor found so far.
  typename std::vector<PolynomialInMatrix>::const_iterator p_best_reductor;
  // All the monomials that need to be reduced
  std::set<Monomial> all_monomials;
  
  for (const auto& current_critical_pair : step_critical_pairs) {
    Polynomial tmp = current_critical_pair.left().second.to_polynomial();
    tmp = tmp * current_critical_pair.left().first;
    for (const auto& term : tmp.terms())
      all_monomials.insert(term.first);
    tmp = current_critical_pair.right().second.to_polynomial();
    tmp = tmp * current_critical_pair.right().first;
    for (const auto& term : tmp.terms())
      all_monomials.insert(term.first);
  }

//  log(LOG_INFO) << all_monomials.size() << " monomials to reduce" << std::endl;

  bool flag;
  while (!all_monomials.empty()) {
//    if (!(all_monomials.size() % 10000))
//      log(LOG_INFO) << " " << all_monomials.size() << " remaining" << std::endl;
    // Selects the largest monomial in all_monomials.
    Monomial m = *(all_monomials.rbegin());
    flag = false;
    for (auto it  = global_reductors.begin();
              it != global_reductors.end();
              ++it) {
      if (m.IsDivisibleBy(it->LeadingMonomial())) {
        if (!flag) {
          flag = true;
          p_best_reductor = it;
          // Strategy used: selects sparsest reductor.
        } else if (it->NumberOfMonomials() < p_best_reductor->NumberOfMonomials()) {
          p_best_reductor = it;
        }
      }
    }
    if (flag) {
      reductor = PolynomialInMatrix(*p_best_reductor);
      auto current_step_reductor = 
        std::pair<Monomial, PolynomialInMatrix>(m/reductor.LeadingMonomial(),
                                                    reductor);
      step_reductors.push_back(current_step_reductor);
      reductor.AddMonomialsMultipliedByCofactor(all_monomials,
                                                m/reductor.LeadingMonomial());
    }
    all_monomials.erase(--all_monomials.end());
  }
//  log(LOG_INFO) << "Time reductors selection: ";
//  log(LOG_INFO) << std::endl; 
  return step_reductors;
}

}  // namespace tinygb
