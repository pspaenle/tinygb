#include "./polynomial_in_matrix.h"

namespace tinygb {

Polynomial PolynomialInMatrix::to_polynomial() const {
  Polynomial P;
  P.SetToZero();
  P.insert(pMatrix_->LeadingMonomial(row_), 1);
  for (unsigned j = 0; j < pMatrix_->column_size(); ++j)
    if (!(*pMatrix_)(row_, j).is_zero())
      P.insert(pMatrix_->ColumnMonomial(j), (*pMatrix_)(row_, j));
  P.Normalize();
  P.DivideByLeadingCoefficient();
  return P;
}

std::ostream& operator<<(std::ostream &o, const PolynomialInMatrix &P) {
  Polynomial p = P.to_polynomial();
  return o << p;
}

std::vector<PolynomialInMatrix>
PolynomialInMatrix::MakeSystemOfPolynomialsInMatrix(const std::shared_ptr<const StorageMatrix>& pM) {
  std::vector<PolynomialInMatrix> system;
  PolynomialInMatrix polynomial;
  for (std::size_t i = 0; i < pM->row_size(); ++i) {
    polynomial = PolynomialInMatrix(pM, i);
    system.push_back(polynomial);
  }
  return system;
}

std::list<Monomial> PolynomialInMatrix::GetMonomials() const {
  std::list<Monomial> result;
  result.push_back(pMatrix_->LeadingMonomial(row_));
  for (std::size_t j = 0; j < pMatrix_->column_size(); ++j)
    if (!(*pMatrix_)(row_, j).is_zero())
      result.push_back(pMatrix_->ColumnMonomial(j));
  return result;
}

void PolynomialInMatrix::AddMonomialsMultipliedByCofactor(
    std::set<Monomial> &monomial_set,
    const Monomial &factor) const {
  Monomial current_monomial;
  for (std::size_t j = 0; j < pMatrix_->column_size(); ++j)
    if (!(*pMatrix_)(row_, j).is_zero()) {
      current_monomial = Monomial(pMatrix_->ColumnMonomial(j));
      current_monomial *= factor;
      monomial_set.insert(current_monomial);
    }
}

bool PolynomialInMatrix::IsTopReducible(
    const std::vector<PolynomialInMatrix>& system) const {
  for (std::size_t i = 0; i < system.size(); ++i)
    if (LeadingMonomial().IsDivisibleBy(system[i].LeadingMonomial()))
      return true;
  return false;
}

bool operator<(const std::pair<Monomial, PolynomialInMatrix>& cp1,
               const std::pair<Monomial, PolynomialInMatrix>& cp2) {
  if (cp1.first < cp2.first) return true;
  if (cp2.first < cp1.first) return false;
  if (cp1.second.pMatrix_ < cp2.second.pMatrix_) return true;
  if (cp2.second.pMatrix_ < cp1.second.pMatrix_) return false;
  if (cp1.second.row_ < cp2.second.row_) return true;
  if (cp2.second.row_ < cp1.second.row_) return false;
  if (cp1.second.LeadingMonomial() < cp2.second.LeadingMonomial()) 
    return true;
  if (cp2.second.LeadingMonomial() < cp1.second.LeadingMonomial()) 
    return false;
  return false;
}

}  // namespace tinygb
