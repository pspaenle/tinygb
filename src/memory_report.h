#ifndef __MEMORY_REPORT_H__
#define __MEMORY_REPORT_H__

#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <string.h>  //for strlen and strncmp
#include <ratio>

namespace tinygb {
namespace memory {

template <class Rep, class MemChunk = std::ratio<1024> >
class Memory_t {
 public:
  Memory_t(const Rep& amount) : amount_(amount) {}
  Rep count() const { return amount_; }

  static constexpr std::intmax_t ratio_num = MemChunk::num;
  static constexpr std::intmax_t ratio_den = MemChunk::den;

 private:
  Rep amount_;
};

typedef Memory_t<long, std::ratio<1024> > kilobytes;
typedef Memory_t<long, std::ratio<1048576> > megabytes;
typedef Memory_t<long, std::ratio<1073741824> > gigabytes;

template <class ToMem_t, class Rep, class MemChunk>
constexpr ToMem_t memory_cast(const Memory_t<Rep, MemChunk>& m) {
  return ToMem_t(ToMem_t::ratio_den * MemChunk::num * m.count() / ToMem_t::ratio_num / MemChunk::den);
}

template <class P>
std::string MemUnit() {
  return "";
}

template <>
std::string MemUnit<kilobytes>() {
  return "kB";
}

template <>
std::string MemUnit<megabytes>() {
  return "MB";
}

template <>
std::string MemUnit<gigabytes>() {
  return "GB";
}

template <class Mem_t = kilobytes>
std::ostream& PrintMem(std::ostream& o, const Mem_t& m) {
  return o << memory_cast<Mem_t>(m).count() << MemUnit<Mem_t>();
}

unsigned long ParseLineProc(char* line){
  int i = strlen(line);
  char* p = line;
  while (*p <'0' || *p > '9') p++;
  char* p2 = p;
  while (*p2 >= '0' && *p2 <= '9') p2++;
  *p2 = '\0';
  i = atoi(p);
  return i;
}

kilobytes getVmSize(){
  FILE* file = fopen("/proc/self/status", "r");
  char line[128];
  unsigned long result = 0;
  while (fgets(line, 128, file) != nullptr){
    if (strncmp(line, "VmSize:", 7) == 0){
      result = ParseLineProc(line);
      break;
    }
  }
  fclose(file);
  return kilobytes(result);
}

kilobytes getVmPeak(){
  FILE* file = fopen("/proc/self/status", "r");
  char line[128];
  unsigned long result = 0;
  while (fgets(line, 128, file) != nullptr){
    if (strncmp(line, "VmPeak:", 7) == 0){
      result = ParseLineProc(line);
      break;
    }
  }
  fclose(file);
  return kilobytes(result);
}

kilobytes getVmRSS(){
  FILE* file = fopen("/proc/self/status", "r");
  char line[128];
  unsigned long result = 0;
  while (fgets(line, 128, file) != nullptr){
    if (strncmp(line, "VmRSS:", 6) == 0){
      result = ParseLineProc(line);
      break;
    }
  }
  fclose(file);
  return kilobytes(result);
}
}  // namespace memory
}  // namespace tinygb

#endif
