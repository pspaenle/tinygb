#define TINYGB_EXECUTABLE

#include "./glob_params.h"
#include "./matrices/rowmajmat.h"
#include "./finite_field_arithmetic/givaro_wrapper.h"
#include "./common.h"
#include "./utils.h"
#include "./select_reductors.h"
#include "./macros.h"
#include "./logger.h"

namespace tinygb {

void selred(const std::string& critpairs_file,
            const std::string& output_file,
            const std::vector<std::string>& mat_ids) {
  std::map<std::string, std::shared_ptr<StorageMatrix>> UniqueMatIds;
  std::vector<CriticalPair> VecCrit;
  TINYGB_TRY(LoadCritPairs(critpairs_file, VecCrit, UniqueMatIds), "selred");
  
  std::vector<PolynomialInMatrix> global_reductors;

  std::shared_ptr<RowMajMat> pM;
  for (const auto& it : mat_ids) {
    TINYGB_TRY(ReadRowMajMat(&pM, "matrices/" + it + ".mat"), "selred");
    for (std::size_t i = 0; i < pM->row_size(); ++i)
      global_reductors.push_back(PolynomialInMatrix{
          std::make_shared<StorageMatrix>(pM, pM->col_dec_, pM->row_dec_, it), i});
  }

  std::vector<std::pair<Monomial, PolynomialInMatrix>> 
    reductors = SelectReductors(VecCrit, global_reductors); 

  TINYGB_TRY(SaveReductors(output_file, reductors), "selred");
}

}  // namespace tinygb

int main(int argc, char **argv) {
  tinygb::Logger("/tmp/log_tinygb").log("selred    start");

  (void)argc; // To get rid of the warning -Wunused-parameter argc

  tinygb::WORKDIR = (std::string)argv[1];

  TINYGB_TRY(tinygb::load_glob_params(tinygb::WORKDIR + "/global_parameters.txt"), "selred");

  std::vector<std::string> mat_ids;
  for (int i = 4; i < argc; ++i)
    mat_ids.push_back(argv[i]);

  tinygb::selred(argv[2], argv[3], mat_ids);
  tinygb::Logger("/tmp/log_tinygb").log("selred    stop");
  return EXIT_SUCCESS;
}

