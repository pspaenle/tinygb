#define TINYGB_EXECUTABLE

#include "./glob_params.h"
#include "./finite_field_arithmetic/givaro_wrapper.h"
#include "./matrices/rowmajmat.h"
#include "./macros.h"
#include "./logger.h"
#include <iostream>

namespace tinygb {

// Compute the row echelon form of D, and reduce B accordingly.
void 
rowech(std::string resD_file, std::string resB_file, 
       std::string matD_file, std::string matB_file) {

  std::shared_ptr<RowMajMat> pB = nullptr, pD = nullptr;

  ReadRowMajMat(&pD, matD_file);
  ReadRowMajMat(&pB, matB_file);

  std::list<std::size_t> pivots; 
  pD->RowEchelon(pB, pivots);
  RowMajMat::Compress(pD, pB, pivots);

  WriteRowMajMat(*pD, resD_file);
  WriteRowMajMat(*pB, resB_file);
}

}  // namespace tinygb


int
main(int argc, char **argv) {
  tinygb::Logger("/tmp/log_tinygb").log("rowech    start");

  (void)argc; // To get rid of the warning -Wunused-parameter argc

  tinygb::WORKDIR = (std::string)argv[1];

  TINYGB_TRY(tinygb::load_glob_params(tinygb::WORKDIR + "/global_parameters.txt"), "rowech");

  tinygb::rowech(argv[2], argv[3], argv[4], argv[5]);
  tinygb::Logger("/tmp/log_tinygb").log("rowech    stop");
  return EXIT_SUCCESS;
}

