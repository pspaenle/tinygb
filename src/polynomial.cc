#include "./polynomial.h"

namespace tinygb {

void Polynomial::CombineMonomials() {
  typename std::list<std::pair<Monomial, GivaroWrapper> >::iterator it2;
  for (auto it1 = terms_.begin(); it1 != terms_.end();) {
    it2 = it1;
    ++it1;
    if (it1 == terms_.end()) {
      break;
    } else if (it1->first == it2->first) {
      it1->second += it2->second;
      terms_.erase(it2);
    }
  }
}

void Polynomial::DivideByLeadingCoefficient() {
  GivaroWrapper leading_coefficient = terms_.back().second;
  assert(leading_coefficient != GivaroWrapper(0));
  leading_coefficient.inv();
  if (leading_coefficient == GivaroWrapper(1)) return;
  for (auto& it : terms_)
    it.second *= leading_coefficient;
}

void Polynomial::Normalize() {
  terms_.sort();
  CombineMonomials();
  RemoveZeroTerms();
}

bool Polynomial::TopReductionBy(const Polynomial &g) {
  assert(!g.IsEmpty());
  if (IsEmpty()) return false;
  if (!LeadingMonomial().IsDivisibleBy(g.LeadingMonomial())) return false;
  Polynomial reductor = g*(LeadingMonomial()/g.LeadingMonomial());
  reductor.DivideByLeadingCoefficient();
  reductor *= LeadingCoefficient();
  operator-=(reductor);
  return true;
}

void Polynomial::RemoveZeroTerms() {
  for (auto it = terms_.begin(); it != terms_.end(); ++it) {
    while (it != terms_.end() && it->second == GivaroWrapper(0))
      it = terms_.erase(it);
  }
}

Polynomial Polynomial::operator*(const Monomial &m) const {
  Polynomial R;
  R.terms_.insert(R.terms_.begin(), this->terms_.begin(), this->terms_.end());
  for (auto& it : R.terms_)
    it.first *= m;
  return R;
}

void Polynomial::operator+=(const Polynomial &P) {
  std::list<Term> tmp_terms = P.terms_;
  terms_.merge(tmp_terms);
  CombineMonomials();
  RemoveZeroTerms();
}

void Polynomial::operator*=(const GivaroWrapper &e) {
  for (auto& it : terms_)
    it.second *= e;
}

void Polynomial::operator/=(const GivaroWrapper& e) {
  for (auto& it : terms_)
    it.second /= e;
}

void Polynomial::operator-=(const Polynomial& P) {
  std::list<Term> tmp_terms = P.terms_;
  for (auto& it : tmp_terms)
    it.second.neg();
  terms_.merge(tmp_terms);
  CombineMonomials();
  RemoveZeroTerms();
}

void Polynomial::Print(std::ostream& o) const {
  for (const auto& it : terms_) {
    o << it.second << " ";
    it.first.Print(o);
    o << std::endl;
  }
  o << ";";
}

std::ostream& operator<<(std::ostream& o, const Polynomial& P) {
  if (P.IsEmpty()) {
    o << "0";
    return o;
  }
  // TODO(pj) : improve printing of polynomial and monomial
  typename std::list<typename Polynomial::Term>::const_iterator it = P.terms_.begin();
  if (it == P.terms_.end())
    return o;
  if (it->second != GivaroWrapper(0))
    o << it->second << " " << it->first;
  ++it;
  for (; it != P.terms_.end() ; ++it)
    if (it->second != GivaroWrapper(0))
      o << "+" << it->second << " " <<  it->first;
  return o;
}

}  // namespace tinygb
