#include "./matrices/io_utils.h"

namespace tinygb {

void uint32_to_char(char* tc, std::uint32_t x) 
{
  for (std::size_t i = 0; i < 4; ++i)
    tc[i] = (char)((x >> (8*i)) & 0xFF);
}

std::uint32_t uchar_to_uint32(const unsigned char* tc) 
{
  std::uint32_t x = 0;
  for (std::size_t i = 0; i < 4; ++i)
    x += (static_cast<std::uint32_t>(tc[i]) << (8*i));
  return x;
}

void uint64_to_char(char* tc, std::uint64_t x) 
{
  for (std::size_t i = 0; i < 8; ++i)
    tc[i] = (char)((x >> (8*i)) & 0xFF);
}

std::uint64_t uchar_to_uint64(const unsigned char* tc) 
{
  std::uint64_t x = 0;
  for (std::size_t i = 0; i < 8; ++i)
    x += (static_cast<std::uint64_t>(tc[i]) << (8*i));
  return x;
}

// only convert the B least significant bytes. Used for compressing elements of
// fields.
void uint64_to_char_trunc(char* tc, std::uint64_t x, std::size_t B) 
{
  for (std::size_t i = 0; i < B; ++i)
    tc[i] = (char)((x >> (8*i)) & 0xFF);
}

std::uint64_t uchar_to_uint64_trunc(const unsigned char* tc, std::size_t B) 
{
  std::uint64_t x = 0;
  for (std::size_t i = 0; i < B; ++i)
    x += (static_cast<std::uint64_t>(tc[i]) << (8*i));
  return x;
}

void write_entries_rowmajmat(std::ofstream& file, 
    const GFp_Givaro::ring_t::Element* t, 
    std::size_t n,
    std::size_t B)
{
  char *arrayB = new char[B];
  for (std::size_t i = 0; i < n; ++i)
  {
    uint64_to_char_trunc(arrayB, (std::uint64_t)t[i], B);
    file.write(arrayB, B);
  }
  delete[] arrayB;
}

void read_entries_rowmajmat(const unsigned char* array, 
    GFp_Givaro::ring_t::Element* t, 
    std::size_t n,
    std::size_t B)
{
  for (std::size_t i = 0; i < n; ++i)
    t[i] = uchar_to_uint64_trunc(array + i*B, B);
}

}  // namespace tinygb


