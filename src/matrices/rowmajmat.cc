#include "./matrices/rowmajmat.h"
#include "./matrices/io_utils.h"
#include "./utils.h"

namespace tinygb {

void RowMajMat::RowEchelon(
    std::shared_ptr<RowMajMat> B,
    std::list<std::size_t> &pivots) {
  pivots.clear();

  std::size_t *Plas = new std::size_t[row_size_];
  std::size_t *Qlas = new std::size_t[column_size_];
  std::size_t rankD;

  std::cerr << "starting row echelon computation" << std::endl;
  rankD = FFPACK::ReducedRowEchelonForm(
      GFp_Givaro::k, row_size_, column_size_, this->entries_, column_size_, Plas, Qlas, false, FFPACK::FfpackSlabRecursive);
  std::cerr << "   -> done" << std::endl;

  for (std::size_t j = 0; j < rankD; ++j)
    for (std::size_t i = 0; i < row_size_; ++i) {
      GFp_Givaro::k.assign(*(this->entries_ + i*column_size_ + j), GFp_Givaro::k.zero);
      if (i == j)
        GFp_Givaro::k.assign(*(this->entries_ + i*column_size_ + j), GFp_Givaro::k.one);
    } 

  GFp_Givaro::ring_t::Element tmpgiv;
  GFp_Givaro::k.init(tmpgiv);

  // TODO(pj): try TileRecursive?

  if (B != nullptr && B->row_size() && rankD > 0) {
    GFp_Givaro::ring_t::Element *valE = FFLAS::fflas_new(GFp_Givaro::k, B->row_size_, rankD);
    GFp_Givaro::ring_t::Element *valF = FFLAS::fflas_new(GFp_Givaro::k, B->row_size_, column_size_-rankD);
    GFp_Givaro::ring_t::Element *valG = FFLAS::fflas_new(GFp_Givaro::k, rankD, column_size_-rankD);

    FFPACK::applyP(GFp_Givaro::k,
        FFLAS::FflasRight,
        FFLAS::FflasTrans,
        B->row_size_, 0, rankD,
        B->entries_, column_size_, Qlas );

    for (std::size_t i = 0; i < B->row_size_; ++i) {
      FFLAS::fassign(
          GFp_Givaro::k, rankD, 
          B->entries_ + i*column_size_, 1,
          valE + i*rankD, 1);
      FFLAS::fassign(
          GFp_Givaro::k, column_size_-rankD, 
          B->entries_ + i*column_size_ + rankD, 1,
          valF + i*(column_size_ - rankD), 1);
    }


    for (std::size_t i = 0; i < rankD; ++i)
      FFLAS::fassign(
          GFp_Givaro::k, column_size_-rankD, 
          this->entries_ + i*column_size_ + rankD, 1,
          valG + i*(column_size_ - rankD), 1);

    // TODO(pj): valD should be deleted here.
    FFLAS::fgemm(GFp_Givaro::k,
        FFLAS::FflasNoTrans,
        FFLAS::FflasNoTrans,
        B->row_size_,
        column_size_ - rankD,
        rankD,
        GFp_Givaro::k.mOne,
        valE, rankD,
        valG, column_size_-rankD,
        GFp_Givaro::k.one,
        valF, column_size_-rankD);

    for (std::size_t i = 0; i < B->row_size_; ++i) {
      FFLAS::fzero(GFp_Givaro::k, rankD, B->entries_ + i*B->column_size_, 1);
      FFLAS::fassign(GFp_Givaro::k, B->column_size_ - rankD, 
          valF + i*(B->column_size_ - rankD), 1,
          B->entries_ + i*B->column_size_ + rankD, 1);
    }

    FFPACK::applyP(GFp_Givaro::k,
        FFLAS::FflasRight,
        FFLAS::FflasNoTrans,
        B->row_size_, 0, rankD,
        B->entries_, column_size_, Qlas );

    FFLAS::fflas_delete(valE);
    FFLAS::fflas_delete(valF);
    FFLAS::fflas_delete(valG);
  }

  for (std::size_t j = 0; j < rankD; ++j)
    pivots.push_back(Qlas[j]);
  FFLAS::fzero(GFp_Givaro::k, column_size_*(row_size_ - rankD),
      this->entries_ + column_size_*rankD, 1);
  FFPACK::applyP(GFp_Givaro::k,
      FFLAS::FflasRight,
      FFLAS::FflasNoTrans,
      row_size_, 0, rankD,
      this->entries_, column_size_, Qlas );

//  log(LOG_INFO) << "reductions to 0:" << row_size() - rankD << std::endl;
  pivots.sort();
  delete Plas;
  delete Qlas;
}

void RowMajMat::MultiplyByInverse(const SpMat& A) {
  GFp_Givaro::ring_t::Element *val_tmp = FFLAS::fflas_new
    (GFp_Givaro::k, SIZE_BLOCK_SPARSE_INV, column_size_);
  std::vector<std::vector<SpMatEntry> > entries_blocks;
  std::size_t nb_blocks = row_size_/SIZE_BLOCK_SPARSE_INV;
  std::size_t rem_block = row_size_ % SIZE_BLOCK_SPARSE_INV;

  // Add extra incomplete block
  if (rem_block) 
    nb_blocks++;
  else
    rem_block = SIZE_BLOCK_SPARSE_INV;
  // the last block has always size rem_block.

  std::size_t total_nb_blocks = ((nb_blocks*(nb_blocks+1))/2);
  entries_blocks.resize(total_nb_blocks);
  for (std::size_t i = 0; i < total_nb_blocks; ++i)
    entries_blocks[i].clear();

  for (const auto& it : A.entries()) {
    std::size_t block_x = it.i_/SIZE_BLOCK_SPARSE_INV;
    std::size_t block_y = it.j_/SIZE_BLOCK_SPARSE_INV;
    std::size_t num_block = 0;
    for (std::size_t i = 0; i < block_x; ++i)
      num_block += nb_blocks - i;
    num_block += (block_y - block_x);
    entries_blocks[num_block].push_back(it); 
  }

  GFp_Givaro::ring_t::Element *diag_block = FFLAS::fflas_new
    (GFp_Givaro::k, SIZE_BLOCK_SPARSE_INV, SIZE_BLOCK_SPARSE_INV);
  GFp_Givaro::ring_t::Element *reduce_block = FFLAS::fflas_new
    (GFp_Givaro::k, SIZE_BLOCK_SPARSE_INV, SIZE_BLOCK_SPARSE_INV);

  for (std::size_t ind = 0; ind < nb_blocks; ++ind) {
    FFLAS::fzero(GFp_Givaro::k, 
        SIZE_BLOCK_SPARSE_INV * SIZE_BLOCK_SPARSE_INV,
        diag_block, 1);
    for (std::size_t i = 0; i < SIZE_BLOCK_SPARSE_INV; ++i)
      GFp_Givaro::k.assign(diag_block[i*SIZE_BLOCK_SPARSE_INV+i], GFp_Givaro::k.one);
    std::size_t num_diag_block = 0;
    for (std::size_t i = 0; i < nb_blocks - 1- ind; ++i)
      num_diag_block += nb_blocks - i;
    for (const auto& it : entries_blocks[num_diag_block]) 
      GFp_Givaro::k.assign(diag_block[(it.i_%SIZE_BLOCK_SPARSE_INV)*SIZE_BLOCK_SPARSE_INV + 
          (it.j_%SIZE_BLOCK_SPARSE_INV)],
          it.v_);

    // ftrtri computes the inverse of a triangular matrix
    FFPACK::ftrtri(GFp_Givaro::k, FFLAS::FflasUpper, FFLAS::FflasUnit, 
        (ind == 0)?rem_block:SIZE_BLOCK_SPARSE_INV,
        diag_block, SIZE_BLOCK_SPARSE_INV);

    FFLAS::fgemm(GFp_Givaro::k,
        FFLAS::FflasNoTrans,
        FFLAS::FflasNoTrans,
        (ind == 0)?rem_block:SIZE_BLOCK_SPARSE_INV,
        column_size_,
        (ind == 0)?rem_block:SIZE_BLOCK_SPARSE_INV,
        GFp_Givaro::k.one,
        diag_block, 
        SIZE_BLOCK_SPARSE_INV,
        this->entries_ + column_size_*(nb_blocks-1-ind)*SIZE_BLOCK_SPARSE_INV, 
        column_size_,
        GFp_Givaro::k.zero,
        val_tmp,
        column_size_);

    for (std::size_t i = 0; i < ((ind == 0)?rem_block:SIZE_BLOCK_SPARSE_INV); ++i)
      FFLAS::fassign(GFp_Givaro::k, column_size_, 
          val_tmp+i*column_size_, 1, 
          this->entries_ + column_size_*(i + (nb_blocks-1-ind)*SIZE_BLOCK_SPARSE_INV), 1);

    for (std::size_t ind2 = 0; ind2 < nb_blocks-1-ind; ++ind2) {
      FFLAS::fzero(GFp_Givaro::k, 
          SIZE_BLOCK_SPARSE_INV * SIZE_BLOCK_SPARSE_INV,
          reduce_block, 1);
      std::size_t num_red_block = 0;
      for (std::size_t i = 0; i < ind2; ++i)
        num_red_block += nb_blocks - i;
      num_red_block += nb_blocks - 1 - ind - ind2;
      if (!entries_blocks[num_red_block].empty()) {
        for (const auto& it : entries_blocks[num_red_block]) 
          GFp_Givaro::k.assign(reduce_block[(it.i_%SIZE_BLOCK_SPARSE_INV)*SIZE_BLOCK_SPARSE_INV + (it.j_%SIZE_BLOCK_SPARSE_INV)],
              it.v_);

        FFLAS::fgemm(GFp_Givaro::k,
            FFLAS::FflasNoTrans,
            FFLAS::FflasNoTrans,
            SIZE_BLOCK_SPARSE_INV,
            column_size_,
            (ind == 0)?rem_block:SIZE_BLOCK_SPARSE_INV,
            GFp_Givaro::k.mOne,
            reduce_block, 
            SIZE_BLOCK_SPARSE_INV,
            this->entries_ + column_size_*(nb_blocks-1-ind)*SIZE_BLOCK_SPARSE_INV, 
            column_size_,
            GFp_Givaro::k.one,
            this->entries_ + column_size_*ind2*SIZE_BLOCK_SPARSE_INV,
            column_size_);
      }
    }
  }

  FFLAS::fflas_delete(diag_block);
  FFLAS::fflas_delete(reduce_block);
  FFLAS::fflas_delete(val_tmp);
}

void RowMajMat::Print() const {
  write_field(
      GFp_Givaro::k, 
      std::cerr, 
      this->entries_, 
      row_size_, 
      column_size_, 
      column_size_) << std::endl;
}

void RowMajMat::Compress() {
//  log(LOG_INFO) << "matrix compression " << row_size_ << " ";
  std::size_t *nbshifts = new std::size_t[row_size_];
  std::size_t tmp = 0, counter = 0;
  for (std::size_t i = 0; i < row_size_; ++i)
    if (IsZeroRow(i)) {
      tmp += 1;
    } else {
      nbshifts[counter] = tmp;
      counter++;
    }
  for (std::size_t i = 0; i < counter; ++i) {
    for (std::size_t l = i*column_size_; l < (i + 1)*column_size_; ++l)
      GFp_Givaro::k.assign(entries_[l], entries_[l + nbshifts[i]*column_size_]);
  }
  row_size_ = counter;
  GFp_Givaro::ring_t::Element *newvals = FFLAS::fflas_new(GFp_Givaro::k, row_size_, column_size_);
  FFLAS::fassign(GFp_Givaro::k, row_size_ * column_size_, entries_, 1, newvals, 1);
  FFLAS::fflas_delete(entries_);
  entries_ = newvals;
//  log(LOG_INFO) << " " << row_size_ << std::endl;
  delete nbshifts;
}

void RowMajMat::RemoveColumns(const std::list<std::size_t> &pivots) {
  if (pivots.size() == 0) return;
  std::list<std::size_t>::const_iterator it = pivots.begin();
  std::size_t pread = 0;
  std::size_t pwrite = 0;

  for (std::size_t nbcol = 0; pread < row_size_*column_size_;) {
    while (nbcol == *it && pread < row_size_*column_size_) {
      ++it;
      if (it == pivots.end())
        it = pivots.begin();
      ++pread;
      ++nbcol;
      if (nbcol == column_size_)
        nbcol = 0;
    }
    if (pread == row_size_*column_size_)
      break;
    GFp_Givaro::k.assign(entries_[pwrite], entries_[pread]);
    ++pwrite;
    ++pread;
    ++nbcol;
    if (nbcol == column_size_)
      nbcol = 0;
  }
  column_size_ -= pivots.size();
  GFp_Givaro::ring_t::Element *newvals = FFLAS::fflas_new(GFp_Givaro::k, row_size_, column_size_);
  FFLAS::fassign(GFp_Givaro::k, row_size_ * column_size_, entries_, 1, newvals, 1);
  FFLAS::fflas_delete(entries_);
  entries_ = newvals;
}

bool RowMajMat::IsZeroRow(std::size_t numrow) const 
{
  for (std::size_t j = 0; j < column_size_; ++j)
    if (!GFp_Givaro::k.isZero((*this)(numrow, j)))
      return false;
  return true;
}

void WriteRowMajMat(
    const RowMajMat& M, 
    const std::string& filename) {

  std::uint32_t size_k = Log256(FIELD_CARD);

  std::ofstream file(
      WORKDIR + "/" + filename, 
      std::ios::in | std::ios::binary | std::ios::trunc);
  if (!file.is_open()) 
    throw std::runtime_error("Unable to open " + WORKDIR + "/" + filename);
  char array32[4];
  char array64[8];

  uint64_to_char(array64, M.row_size());
  file.write(array64, 8);
  uint64_to_char(array64, M.column_size());
  file.write(array64, 8);

  std::uint64_t index = 8*3 + 4*3; // Three first fields + three tags

  if (!M.col_dec_.empty())
    index += 4 + NB_VARS*M.column_size()*4;
  if (!M.row_dec_.empty())
    index += NB_VARS*M.row_size()*4;
  if (!M.row_tags_.empty())
    index += (NB_VARS + 2)*M.row_size()*4;

  uint64_to_char(array64, index); // after how many bytes does the data for the entries start.
  file.write(array64, 8);

  // First decoration
  if (M.col_dec_.empty()) {
    uint32_to_char(array32, 0);
    file.write(array32, 4);
  } else {
    uint32_to_char(array32, 1);
    file.write(array32, 4);
    uint32_to_char(array32, NB_VARS);
    file.write(array32, 4);
    for (const auto& it : M.col_dec_) 
      for (std::size_t i = 0; i < NB_VARS; ++i) {
        uint32_to_char(array32, it.getexp(i));
        file.write(array32, 4);
      }
  }

  // Second decoration
  if (M.row_dec_.empty()) {
    uint32_to_char(array32, 0);
    file.write(array32, 4);
  } else {
    assert(!M.col_dec_.empty() || M.column_size() == 0);
    uint32_to_char(array32, 1);
    file.write(array32, 4);
    assert(M.row_dec_.size() == M.row_size());
    for (const auto& it : M.row_dec_) 
      for (std::size_t i = 0; i < NB_VARS; ++i) {
        uint32_to_char(array32, it.getexp(i));
        file.write(array32, 4);
      }
  }

  // Third decoration
  if (M.row_tags_.empty()) {
    uint32_to_char(array32, 0);
    file.write(array32, 4);
  } else {
    assert(!M.row_dec_.empty());
    uint32_to_char(array32, 1);
    file.write(array32, 4);
    for (const auto& it : M.row_tags_) {
      uint32_to_char(array32, it.s);
      file.write(array32, 4);
      uint32_to_char(array32, it.n);
      file.write(array32, 4);
      for (std::size_t i = 0; i < NB_VARS; ++i) {
        uint32_to_char(array32, it.m.getexp(i));
        file.write(array32, 4);
      }
    }
  }

  // Writing the entries of the matrix
  write_entries_rowmajmat(file, M.entries(), M.row_size() * M.column_size(), size_k);

  file.close();
}

void ReadRowMajMat(std::shared_ptr<RowMajMat> *pR, const std::string& filename) {
  std::streampos size;
  std::uint32_t size_k     = Log256(FIELD_CARD);

  std::ifstream file(
      WORKDIR + "/" + filename, 
      std::ios::out | std::ios::binary | std::ios::ate);
  unsigned char *memblock, *memblock_begin;
  if (!file.is_open()) 
    throw std::runtime_error("Unable to open " + WORKDIR + "/" + filename);
  size = file.tellg();
  memblock = new unsigned char[size];
  memblock_begin = memblock;
  file.seekg(0, std::ios::beg);
  file.read(reinterpret_cast<char*>(memblock), size);
  file.close();

  std::uint64_t row_dim = uchar_to_uint64(memblock);
  memblock += 8;
  std::uint64_t col_dim = uchar_to_uint64(memblock);
  memblock += 8;

  // Index at which the data for the entries of the matrix start
  std::uint64_t ind = uchar_to_uint64(memblock);
  memblock += 8;
  *pR = std::make_shared<RowMajMat>(row_dim, col_dim);

  // Reading first decoration
  std::uint64_t tag1 = uchar_to_uint32(memblock);
  memblock += 4;
  if (tag1) {
    memblock += 4; // skipping the number of variables which is already known
    expo_int* m = new expo_int[NB_VARS];
    for (std::size_t i = 0; i < col_dim; ++i) {
      for (std::size_t j = 0; j < NB_VARS; ++j) {
        m[j] = uchar_to_uint32(memblock);
        memblock += 4;
      }
      (*pR)->col_dec_.push_back(Monomial(m));
    }
    delete[] m;
  }

  // Reading second decoration
  std::uint64_t tag2 = uchar_to_uint32(memblock);
  memblock += 4;
  if ((tag1 || !col_dim) && tag2) {
    expo_int* m = new expo_int[NB_VARS];
    for (std::size_t i = 0; i < row_dim; ++i)
    {
      for (std::size_t j = 0; j < NB_VARS; ++j)
      {
        m[j] = uchar_to_uint32(memblock);
        memblock += 4;
      }
      (*pR)->row_dec_.push_back(Monomial(m));
    }
    delete[] m;
  }

  // Reading third decoration
  std::uint64_t tag3 = uchar_to_uint32(memblock);
  memblock += 4;
  if ((tag1 || !col_dim) && tag2 && tag3) 
  {
    expo_int* m = new expo_int[NB_VARS];
    RowTag t;
    for (std::size_t i = 0; i < row_dim; ++i)
    {
      t.s = uchar_to_uint32(memblock);
      memblock += 4;
      t.n = uchar_to_uint32(memblock);
      memblock += 4;

      for (std::size_t j = 0; j < NB_VARS; ++j)
      {
        m[j] = uchar_to_uint32(memblock);
        memblock += 4;
      }
      t.m = Monomial(m);
      (*pR)->row_tags_.push_back(t);
    }
    delete m;
  }

  // Reading entries of the matrix
  memblock = memblock_begin + ind;
  read_entries_rowmajmat(memblock, 
      (*pR)->entries_, 
      row_dim*col_dim,
      size_k);

  delete[] memblock_begin;
}

void 
RowMajMat::Compress(std::shared_ptr<RowMajMat> D, std::shared_ptr<RowMajMat> B, std::list<std::size_t> &pivots) {
  assert(D->col_dec_ == B->col_dec_);
  assert(!B->col_dec_.empty() || !B->column_size());
  
  std::vector<Monomial> non_pivot_monomials;

  pivots.sort();

  std::list<std::size_t>::const_iterator it = pivots.begin();
  for (std::size_t i = 0; i < B->col_dec_.size(); ++i)
    if ((i != *it) || (it == pivots.end()))
      non_pivot_monomials.push_back(B->col_dec_[i]);
    else
      it++;
  if (D != nullptr) 
  {
    for (const auto& it : pivots)
      D->row_dec_.push_back(D->col_dec_[it]);
    D->Compress();
    D->RemoveColumns(pivots);
  }
  if (B != nullptr)
    B->RemoveColumns(pivots);
  
  B->col_dec_ = non_pivot_monomials;
  D->col_dec_ = non_pivot_monomials;
}

}  // namespace tinygb
