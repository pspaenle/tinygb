This file describes the data structures for matrices stored in row major form
(extension .rmm) in binary files.

A file with extension .rmm describes a matrix over a prime finite field Z/pZ,
with possible decorations of columns and rows. Extensions describe the
decorations: .rmm1 means that it has decoration 1), .rmm2 means that it has
decorations 1) and 2), and .rmm3 means that it has decorations 1), 2) and 3).

There are three possible decorations :
  1) Columns are labelled by monomials
  2) Rows are labelled by monomials 
  3) Rows are labelled by a triplet (s, n, m), where s and n are integers and m
is a monomial

These decorations correspond to the following usage :
lines of these matrices correspond to polynomials, written in a monomial basis.
With decoration 1) alone, the polynomial corresponding to a row is the sum of
the entries of the row multiplied by the tag corresponding to the column.

If decoration 2) is enabled, the polynomial corresponding to a row is monic,
with leading monomial given by the tag of the row. Decoration 2 is useful when
the matrix is in row echelon form in order to avoid storing the identity block
containing the pivots.

Finally, decoration 3) records the origin of the polynomial corresponding to the
row. It means that the row has been obtained by reducing the polynomial
corresponding to the row n of the matrix computed at step s, multiplied by the
monomial m.

The data structure used to represent these matrices is:
- [row_dim]: 64 bits representing the row dimension of the matrix
- [col_dim]: 64 bits representing the column dimension of the matrix
- [ind] : 64 bits indicating after how many bytes the data representing the
  entries of the matrix start

- Decoration 1)
  - [tag1]: 32 bits, 0 or 1 depending on whether the first decoration is used.
    The following fields are represented only if the tag is set to 1.
  - [nb_vars]: 32 bits representing the number of variables in the monomials.
  - [list_mons1]: the list of monomials, given by their exponent vectors
    encoded on 32 bits (i.e. [col_dim]*[nb_vars]*32 bits)

Decoration 2)
  - [tag2]: 32 bits, 0 or 1 depending on whether the first decoration is used.
    The following fields are represented only if the tag is set to 1.
  - [list_mons2]: the list of monomials, given by their exponent vectors
    encoded on 32 bits (i.e. [row_dim]*[nb_vars]*32 bits)
  
Decoration 3)
  - [tag3]: 32 bits, 0 or 1 depending on whether the first decoration is used.
    The following fields are represented only if the tag is set to 1.
  - [list_tags]: the list of triplets (s,n,m), where s and n are encoded on 32
    bits (i.e. [row_dim]*32*(2+[nb_vars]))

- [entries], the list of entries, in row major order, i.e.
  [row_dim]*[col_dim]*[size_card] bits, where [size_card] is the number of
bytes required to represent an element in Z/pZ, i.e. the ceiling of
log(p)/log(256);
