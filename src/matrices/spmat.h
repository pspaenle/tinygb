// This class implements sparse upper triangular matrices

#ifndef __SPMAT_H__
#define __SPMAT_H__

#include "./common.h"
#include "finite_field_arithmetic/givaro_wrapper.h"
#include <memory>

namespace tinygb {
  
class SpMatEntry {
 public:
  std::size_t i_;
  std::size_t j_;
  GFp_Givaro::ring_t::Element v_;

  SpMatEntry(): i_(0), j_(0) {}
  SpMatEntry(std::size_t i, std::size_t j, const GFp_Givaro::ring_t::Element &v)
    : i_(i), j_(j) { GFp_Givaro::k.assign(v_, v); }
};

// TODO: we only need square matrices
class SpMat {
 public:
  SpMat(): row_size_(0), column_size_(0) {}
  SpMat(std::size_t row_size, std::size_t col_size)
    : row_size_(row_size), column_size_(col_size) {}

  const std::vector<std::size_t>& NumberOfNonzeroEltsByRow() const {
    return nb_nonzero_elts_by_row_;
  }

  std::size_t row_size() const {
    return row_size_;
  }
  std::size_t column_size() const {
    return column_size_;
  }

  const std::vector<SpMatEntry>& entries() const {
    return entries_;
  }

  unsigned long NumberOfNonzeroEntries() const {
    return entries_.size();
  }

  std::size_t NumberOfNonzeroEntriesOnRow(std::size_t n) const {
    return nb_nonzero_elts_by_row_[n];
  }

  // for square upper triangular with ones on the diagonal
  double density() const {
    std::size_t nb_nonzero_elts = 0;
    for (std::size_t i = 0; i < row_size(); ++i)
      nb_nonzero_elts += nb_nonzero_elts_by_row_[i];
    return (double)nb_nonzero_elts/((double)row_size()*(row_size()-1))*2;
  }

  void Print() const;

  // TODO(pj): should be private
  std::size_t row_size_;
  std::size_t column_size_;

 private:

  //TODO(pj): Warning: not counting the diagonal
  std::vector<std::size_t> nb_nonzero_elts_by_row_;

  std::vector<SpMatEntry> entries_;

  friend class MatrixFactory;

  friend void ReadSpMat(std::shared_ptr<SpMat> *pR, const std::string& filename);
  friend void WriteSpMat(const SpMat& M, const std::string& filename);
};

}  // namespace tinygb

#endif
