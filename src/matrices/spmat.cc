#include <fstream>
#include "matrices/spmat.h"
#include "./glob_params.h"

namespace tinygb {

void SpMat::Print() const {
//  log(LOG_INFO) << "dim: " << row_size_ << std::endl;
//  for (const auto& it : entries_)
//    log(LOG_INFO) << it.i_ << " " << it.j_ << " " << std::endl;
} 

void WriteSpMat(const SpMat& M, const std::string& filename) {
  std::ofstream file(
      WORKDIR + "/" + filename, 
      std::ios::in | std::ios::trunc);
  if (!file.is_open()) 
    throw std::runtime_error("Unable to open file " + filename);

  file << M.row_size() << " " << M.column_size() << " " << M.NumberOfNonzeroEntries() << std::endl;
  for (const auto& it : M.entries())
    file << it.i_ << " " << it.j_ << " " << it.v_ << std::endl;
  file.close();
}

void ReadSpMat(std::shared_ptr<SpMat> *pR, const std::string& filename) {
  std::ifstream file(
      WORKDIR + "/" + filename, 
      std::ios::out);
  if (!file.is_open()) 
    throw std::runtime_error("Unable to open file " + filename);
  std::size_t row_size = 0, column_size = 0;
  unsigned long nb_entries = 0;
  file >> row_size >> column_size >> nb_entries;

  *pR = std::make_shared<SpMat>(row_size, column_size);

  std::size_t i = 0, j = 0;
  std::uint64_t entry = 0;
  for (unsigned long l = 0; l < nb_entries; ++l) {
    file >> i >> j >> entry;
    (*pR)->entries_.push_back(SpMatEntry(i, j, entry));
  }
  file.close();
}

}  // namespace tinygb
