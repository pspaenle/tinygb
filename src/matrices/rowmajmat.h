#ifndef __ROWMAJMAT_H__
#define __ROWMAJMAT_H__

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wignored-attributes"
#pragma GCC diagnostic ignored "-Wattributes"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include <fflas-ffpack/fflas-ffpack.h>
#pragma GCC diagnostic pop

#include "./monomial.h"
#include "./matrices/spmat.h"

namespace tinygb {

class RowTag {
 public:
  std::uint32_t s; // step number
  std::uint32_t n; //row number
  Monomial m;
};

class RowMajMat {
 public:
  RowMajMat()
    : row_size_(0), column_size_(0), entries_(nullptr) {}

  RowMajMat(
      std::size_t row_size, 
      std::size_t column_size)
    : row_size_(row_size), column_size_(column_size) {
      entries_ = FFLAS::fflas_new(GFp_Givaro::k, row_size_, column_size_);
    }

  ~RowMajMat() {
    FFLAS::fflas_delete(entries_);
  }

  inline std::size_t row_size() const {
    return row_size_;
  }

  inline std::size_t column_size() const {
    return column_size_;
  }
  
  // TODO(pj): This should not be public.
  GFp_Givaro::ring_t::Element* entries() {
    return entries_;
  }
  
  const GFp_Givaro::ring_t::Element* entries() const {
    return entries_;
  }


  GFp_Givaro::ring_t::Element& operator()(std::size_t i, std::size_t j) {
    assert (i < row_size_);
    assert (j < column_size_);
    return entries_[i*column_size_ + j];
  }

  const GFp_Givaro::ring_t::Element& operator()(std::size_t i, std::size_t j) const {
    assert (i < row_size_);
    assert (j < column_size_);
    return entries_[i*column_size_ + j];
  }

  // Tests if row given by argument is zero.
  bool IsZeroRow(std::size_t) const;

  // Multiplies on the left the argument matrix by the inverse of the sparse
  // triangular matrix.
  void MultiplyByInverse(const SpMat& A);

  void RowEchelon(std::shared_ptr<RowMajMat>, std::list<std::size_t>&);
  void Print() const;
  void Compress();
  void RemoveColumns(const std::list<std::size_t>&);
  void SetEntry(std::size_t i, std::size_t j, const GFp_Givaro::ring_t::Element &v) {
    entries_[i*column_size_ + j] = v;
  }

  double density() const {
    std::size_t nb_nonzero_elts = 0;
    for (std::size_t i = 0; i < row_size(); ++i)
      for (std::size_t j = 0; j < column_size(); ++j)
        if (!GFp_Givaro::k.isZero((*this)(i, j)))
          nb_nonzero_elts++;
    return (double)nb_nonzero_elts/(double)row_size()/(double)column_size();
  }

  static void
  Compress(std::shared_ptr<RowMajMat> D, std::shared_ptr<RowMajMat> B, std::list<std::size_t> &pivots);

  // TODO(pj): should be private
  std::size_t row_size_;
  std::size_t column_size_;
  GFp_Givaro::ring_t::Element* entries_;

  std::vector<Monomial> col_dec_;  // column decorations
  std::vector<Monomial> row_dec_;  // row decorations

  // This third decoration is currently not used
  // TODO : use design pattern Decorator
  std::vector<RowTag>   row_tags_; // row tags
};

void WriteRowMajMat(const RowMajMat& M, const std::string& filename);
void ReadRowMajMat(std::shared_ptr<RowMajMat> *pR, const std::string& filename);


}  // namespace tinygb

#endif
