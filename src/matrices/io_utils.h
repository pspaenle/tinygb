#ifndef __IO_UTILS_H__
#define __IO_UTILS_H__

#include <cstdint>
#include <fstream>
#include "finite_field_arithmetic/givaro_wrapper.h"

namespace tinygb {

void uint32_to_char(char* tc, std::uint32_t x);
std::uint32_t uchar_to_uint32(const unsigned char* tc);

void uint64_to_char(char* tc, std::uint64_t x);
std::uint64_t uchar_to_uint64(const unsigned char* tc);

// only convert the B least significant bytes. Used for compressing elements of
// fields.
void uint64_to_char_trunc(char* tc, std::uint64_t x, std::size_t B);
std::uint64_t uchar_to_uint64_trunc(const unsigned char* tc, std::size_t B);

void write_entries_rowmajmat(std::ofstream& file, 
    const GFp_Givaro::ring_t::Element* t, 
    std::size_t n,
    std::size_t B);

//TODO: not very clean, should take an std::ifstream as main arg.
void read_entries_rowmajmat(const unsigned char* array, 
    GFp_Givaro::ring_t::Element* t, 
    std::size_t n,
    std::size_t B);
}  // namespace tinygb

#endif
