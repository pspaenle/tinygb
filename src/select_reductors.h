#ifndef __SELECT_REDUCTORS_H__
#define __SELECT_REDUCTORS_H__

#include <vector>
#include "./critical_pair.h"
#include "./polynomial_in_matrix.h"
#include "./monomial.h"

namespace tinygb {

// Selects reductors to reduce a list of critical pairs. step_critical_pairs
// contains the list of critical pairs selected for this step of the F4
// algorithm. step_reductors contains the output, i.e. a list of pairs of
// monomials and polynomials which are global reductors.
// TODO(pj): simplify and decompose.
std::vector<std::pair<Monomial, PolynomialInMatrix>> SelectReductors(
    const std::vector<CriticalPair>& step_critical_pairs,
    const std::vector<PolynomialInMatrix>& global_reductors);

}  // namespace tinygb

#endif
