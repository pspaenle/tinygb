#ifndef __LINALG_MATRIX_FACTORY_H__
#define __LINALG_MATRIX_FACTORY_H__

#include <tuple>
#include "./matrices/spmat.h"
#include "./matrices/rowmajmat.h"
#include "./critical_pair.h"
#include "./polynomial_in_matrix.h"
#include "linalg/storage_matrix.h"
#include "./polynomial.h"

namespace tinygb {
// TODO(pj): Remove useless factory constructors?
// TODO(pj): Is the vector of monomials useful in the constructors?
// TODO(pj): Remove code repetition for MatrixRowMajor?

class MatrixFactory {
  public:
  // TODO(pj): Check that no zero element is added
  // TODO(pj): Is the vector of monomials required?
  static std::unique_ptr<SpMat> BuildSparseUpperTriangular(
     const std::vector<std::pair<Monomial, PolynomialInMatrix> >&,
     const std::vector<Monomial>&);

  static std::unique_ptr<RowMajMat> BuildRowMajor(
     const std::vector<std::pair<Monomial, PolynomialInMatrix> >&,
     const std::vector<Monomial>&); 

  static std::unique_ptr<RowMajMat> BuildRowMajor(
      const std::vector<Polynomial>&,
      const std::vector<Monomial>&);
};

std::unique_ptr<SpMat> MatrixFactory::BuildSparseUpperTriangular(
     const std::vector<std::pair<Monomial, PolynomialInMatrix> >& list_reductors,
     const std::vector<Monomial>& list_monomials) {
  auto R = std::make_unique<SpMat>();
  R->nb_nonzero_elts_by_row_.clear();
  R->row_size_ = list_reductors.size();
  R->column_size_ = list_monomials.size();
  R->nb_nonzero_elts_by_row_.assign(R->column_size_, 0);
  Polynomial tmp;
  std::map<Monomial, std::size_t> map_mon;
  for (std::size_t i = 0; i < list_monomials.size(); ++i)
    map_mon.insert(std::pair<Monomial, std::size_t>(list_monomials[i], i));
  R->entries_.clear();
  std::size_t num_pol = R->row_size_ - 1;
  for (typename std::vector<std::pair<Monomial, PolynomialInMatrix>>::const_reverse_iterator it = list_reductors.rbegin(); 
    it != list_reductors.rend(); ++it) {
    tmp = it->second.to_polynomial();
    tmp = tmp*(it->first);
    for (typename std::list<std::pair<Monomial, GivaroWrapper> >::
         const_iterator it2 = tmp.terms().begin();
         it2 != --(tmp.terms().end()); ++it2)
      if (map_mon.count(it2->first)) {
        R->entries_.push_back(SpMatEntry(
              (std::size_t)num_pol, map_mon[it2->first], it2->second.v));
        R->nb_nonzero_elts_by_row_[(std::size_t)num_pol]++;
      }
    num_pol--;
  }
  return R;
}

std::unique_ptr<RowMajMat> MatrixFactory::BuildRowMajor(
    const std::vector<std::pair<Monomial, PolynomialInMatrix> > &list_reductors,
    const std::vector<Monomial> &list_monomials) {
  auto R = std::make_unique<RowMajMat>(list_reductors.size(), list_monomials.size());
  Polynomial tmp;
  std::map<Monomial, std::size_t> map_mon;
  for (std::size_t i = 0; i < list_monomials.size(); ++i)
    map_mon.insert(std::pair<Monomial, std::size_t>(list_monomials[i], i));
  std::size_t num_pol = 0;
  for (const auto& it : list_reductors) {
    tmp = it.second.to_polynomial();
    tmp = tmp*(it.first);
    for (const auto& it2 : tmp.terms())
      if (map_mon.count(it2.first))
        R->SetEntry(num_pol, map_mon[it2.first], it2.second.v);
    num_pol++;
  }
  return R;
}

// TODO: if the input polynomials must be monic, then this should be specified
// in an assert
std::unique_ptr<RowMajMat> MatrixFactory::BuildRowMajor(
    const std::vector<Polynomial>& list_pols,
    const std::vector<Monomial>& list_monomials) {
  auto R = std::make_unique<RowMajMat>(list_pols.size(), list_monomials.size());
  std::map<Monomial, std::size_t> map_mon;
  for (std::size_t i = 0; i < list_monomials.size(); ++i)
    map_mon.insert(std::pair<Monomial, std::size_t>(list_monomials[i], i));
  for (std::size_t num_pol = 0; num_pol < R->row_size_; ++num_pol)
    for (const auto& it : list_pols[num_pol].terms())
      if (map_mon.count(it.first))
        R->SetEntry(num_pol, map_mon[it.first], it.second.v);
  return R;
}

}  // namespace tinygb
#endif  // LINALG_MATRIX_FACTORY_H_

