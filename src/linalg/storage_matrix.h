#ifndef __LINALG_STORAGE_MATRIX_H__
#define __LINALG_STORAGE_MATRIX_H__

#include <memory>
#include <vector>
#include "./glob_params.h"
#include "./matrices/spmat.h"
#include "./matrices/rowmajmat.h"
#include "./utils.h"

namespace tinygb {

class StorageMatrix {
 public:
  StorageMatrix() : id_(RandomString(TINYGB_SIZE_RANDSTR)) {};

  StorageMatrix(const std::shared_ptr<RowMajMat>& M, 
                const std::vector<Monomial>& column_monomials,
                const std::vector<Monomial>& row_leading_monomials,
                std::string id = RandomString(TINYGB_SIZE_RANDSTR)) 
    : id_(id), 
      pMatrix_(M), 
      column_monomials_(column_monomials), 
      row_leading_monomials_(row_leading_monomials) {
        assert(row_size() == row_leading_monomials_.size());
      }

  // Move constructor.
  StorageMatrix(StorageMatrix&& S)
    : id_(RandomString(TINYGB_SIZE_RANDSTR)), 
      pMatrix_(nullptr) 
  {
      column_monomials_ = S.column_monomials_;
      row_leading_monomials_ = S.row_leading_monomials_;
      pMatrix_ = std::move(S.pMatrix_);
      S.column_monomials_.empty();
      S.row_leading_monomials_.empty();
      S.pMatrix_ = nullptr;
  }

  Monomial LeadingMonomial(std::size_t row) const { 
    assert(row < row_leading_monomials_.size());
    return row_leading_monomials_[row];
  }

  std::size_t row_size()    const { return pMatrix_->row_size();    }
  std::size_t column_size() const { return pMatrix_->column_size(); }

  Monomial ColumnMonomial(std::size_t i) const {
    return column_monomials_[i];
  }

  GivaroWrapper operator()(std::size_t i, std::size_t j) const {
    return GivaroWrapper((*pMatrix_)(i, j));
  }

  // FIXME(pj): ostream should be an arg in Print
  void Print() const {
    std::cerr << "columns: " << std::endl;
    for (std::size_t i = 0; i < column_monomials_.size(); ++i)
      std::cerr << column_monomials_[i] << " ";
    std::cerr << std::endl;
    std::cerr << "rows: " << std::endl;
    for (std::size_t i = 0; i < row_leading_monomials_.size(); ++i)
      std::cerr << row_leading_monomials_[i] << " ";
    std::cerr << std::endl;
    pMatrix_->Print();
  }

  void Store(std::string filename) const;

  std::string id() const { return id_; }

// private:
  std::string id_;
  std::shared_ptr<RowMajMat> pMatrix_;
  std::vector<Monomial> column_monomials_;
  std::vector<Monomial> row_leading_monomials_;
};

}  // namespace tinygb
#endif  // LINALG_STORAGE_MATRIX_H_
