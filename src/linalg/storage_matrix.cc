#include "./storage_matrix.h"
#include "./macros.h"

namespace tinygb {

void StorageMatrix::Store(std::string filename) const {
  TINYGB_TRY_AND_THROW(WriteRowMajMat(*pMatrix_, filename), 
                       "fn StorageMatrix::Store", 
                       "Unable to store matrix");
}

}  // namespace tinygb
