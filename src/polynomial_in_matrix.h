#ifndef __POLYNOMIAL_IN_MATRIX_H__
#define __POLYNOMIAL_IN_MATRIX_H__

#include <map>
#include <set>
#include <utility>
#include <vector>
#include "./common.h"
#include "finite_field_arithmetic/givaro_wrapper.h"
#include "./monomial.h"
#include "./polynomial.h"
#include "linalg/storage_matrix.h"

namespace tinygb {

class PolynomialInMatrix : public AbstractPolynomial<GFp_Givaro, PolynomialInMatrix> {
 public:
  PolynomialInMatrix() : pMatrix_(nullptr), row_(0), nbMonoms_(0) {}

  PolynomialInMatrix(const std::shared_ptr<const StorageMatrix>& pMatrix, std::size_t Row) 
    : pMatrix_(pMatrix), row_(Row) {
      nbMonoms_ = 1;
      for (std::size_t j = 0; j < pMatrix_->column_size(); ++j) {
        if (!(*pMatrix)(row_, j).is_zero())
          nbMonoms_++;
      }
    }

  PolynomialInMatrix(const PolynomialInMatrix &p) 
    : pMatrix_(p.pMatrix_), row_(p.row_), nbMonoms_(p.nbMonoms_) {}

  PolynomialInMatrix& operator=(const PolynomialInMatrix &p) {
    pMatrix_  = p.pMatrix_;
    row_      = p.row_;
    nbMonoms_ = p.nbMonoms_;
    return *this;
  }

  inline Monomial LeadingMonomial() const {
    return pMatrix_->LeadingMonomial(row_);
  }

  Polynomial to_polynomial() const;

  // FIXME(pj): this only works for degree orderings.
  inline unsigned Degree() const {
    return LeadingMonomial().Degree();
  } 

  // Returns true if the object is top reducible by one of the polynomials in
  // the argument.
  bool IsTopReducible(const std::vector<PolynomialInMatrix>&) const;

  // Converts a StorageMatrix to the vector of polynomials
  // corresponding to the list of Rows of the matrix
  static std::vector<PolynomialInMatrix>
    MakeSystemOfPolynomialsInMatrix(const std::shared_ptr<const StorageMatrix>&);

  // Merges the first argument with the set of monomials occuring with nonzero
  // coefficient in the polynomial (with the exception of the leading monomial)
  // multiplied by the second argument.
  void AddMonomialsMultipliedByCofactor(std::set<Monomial>&, const Monomial&) const;

  // Returns the number of monomials with nonzero coefficient in the polynomial.
  std::size_t NumberOfMonomials() const { return nbMonoms_; }

  // Returns the list of monomials which occur with nonzero coefficient in the
  // polynomial.
  std::list<Monomial> GetMonomials() const;

  std::shared_ptr<const StorageMatrix> pMatrix() const { return pMatrix_; }
  std::size_t row() const { return row_; }

 private:
  std::shared_ptr<const StorageMatrix> pMatrix_;
  std::size_t row_;
  std::size_t nbMonoms_;

  friend std::ostream& operator<<(std::ostream&, const PolynomialInMatrix&);

  friend bool operator<(const std::pair<Monomial, PolynomialInMatrix>&,
                        const std::pair<Monomial, PolynomialInMatrix>&);
};

}  // namespace tinygb

#endif  // POLYNOMIAL_IN_MATRIX_H_
