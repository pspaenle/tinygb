#ifndef __COMMON_H__
#define __COMMON_H__

#include <gmp.h>
#include <string>

namespace tinygb {

/* Constant values
 *****************************************************************************/

// 18 is such that the proba that two randomly generated strings with 18 lower
// case characters have a probability < 2^(-80) to collide. This is due to the
// inequality 26^18 > 2^80.

const std::size_t TINYGB_SIZE_RANDSTR = 18;

// Size of blocks for utsolve.
// This should probably be moved to the utsolve directory.
#define SIZE_BLOCK_SPARSE_INV 50

}  // namespace tinygb
#endif  // COMMON_H_
