#define TINYGB_EXECUTABLE

#include "./polynomial.h"
#include "matrices/rowmajmat.h"
#include "./parser.h"
#include "linalg/storage_matrix.h"
#include "linalg/matrix_factory.h"
#include "./macros.h"
#include "./logger.h"

namespace tinygb {

// Interreduces a list of polynomials (given via the variable system) using
// linear algebra.  The result is stored in an F4 matrix which is added to list_matrices and
// a list of pointers to the corresponding polynomials are returned.
std::shared_ptr<const StorageMatrix> Interreduce(const std::vector<Polynomial>& system) {

  std::set<Monomial> lv;

  for (std::size_t i = 0; i < system.size(); ++i)
    for (const auto& it : system[i].terms())
      lv.insert(it.first);

  std::vector<Monomial> v;
  v.insert(v.begin(), lv.rbegin(), lv.rend());

  std::shared_ptr<RowMajMat> D = MatrixFactory::BuildRowMajor(system, v);

  auto B = std::make_shared<RowMajMat>(0, D->column_size());

  B->col_dec_ = v;
  D->col_dec_ = v;

  std::list<std::size_t> pivots; 
  
  D->RowEchelon(B, pivots);

  RowMajMat::Compress(D, B, pivots);

  auto p_current_storage_matrix = std::make_shared<const StorageMatrix>(D, D->col_dec_, D->row_dec_, "interreduced_system");

  p_current_storage_matrix->Store("matrices/" + p_current_storage_matrix->id() + ".mat");
  return p_current_storage_matrix;
}

void tinygb_init(std::vector<Polynomial>& sys) {
  std::vector<PolynomialInMatrix> gb;
  std::vector<CriticalPair>       global_critical_pairs;
  std::vector<CriticalPair>       step_critical_pairs;
  std::vector<PolynomialInMatrix> global_reductors;

  std::map<std::string, std::shared_ptr<StorageMatrix>> UniqueMatIds;

  std::shared_ptr<const StorageMatrix> gb_init = Interreduce(sys);
  
  // saves an empty list of critical pairs and gb.
  TINYGB_TRY(SaveCritPairs("critpairs_glob.txt", global_critical_pairs), 
             "tinygb_init");
  TINYGB_TRY(SaveGB("gb.txt", gb), 
             "tinygb_init");
}

}  // namespace tinygb

int main(int argc, char **argv) {
  tinygb::Logger("/tmp/log_tinygb").log("tinygb_init start");
  (void)argc; // To get rid of the warning -Wunused-parameter argc

  tinygb::WORKDIR = (std::string)argv[1];

  std::ifstream file;
  TINYGB_TRY(file = std::ifstream((std::string)argv[2], std::ios::out), "tinygb_init");
 
  file >> tinygb::NB_VARS;
  file >> tinygb::FIELD_CARD;
  tinygb::MONOMIAL_ORDERING = "grevlex";  
  std::system(("mkdir -p " + tinygb::WORKDIR + "/matrices").c_str());
  tinygb::GFp_Givaro::k = tinygb::GFp_Givaro::ring_t(Givaro::Integer(tinygb::FIELD_CARD));

  TINYGB_TRY(tinygb::save_glob_params(tinygb::WORKDIR + "/global_parameters.txt"), "tinygb_init");
  
  tinygb::Parser parser(file);
  std::vector<tinygb::Polynomial> sys = parser.parse();
  
  tinygb::tinygb_init(sys);

  TINYGB_TRY(file.close(), "tinygb_init");

  tinygb::Logger("/tmp/log_tinygb").log("tinygb_init end");
  return EXIT_SUCCESS;
}
