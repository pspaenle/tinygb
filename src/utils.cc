
/**************************************************************************//***
 * \file utils.cc
 * \brief Implementations of various helper functions.
 ******************************************************************************/

#include "./utils.h"
#include "./critical_pair.h"
#include "./polynomial_in_matrix.h"
#include "./macros.h"

namespace tinygb {

std::uint32_t 
Log256(std::uint64_t x) 
{
  std::uint32_t res = 1;  // the result cannot exceed 8
  for (; x >>= 8; res++);
  return res;
}

std::string
RandomString(std::size_t k) 
{
  std::string s;
  for (std::size_t i = 0; i < k; ++i)
    s.push_back('a' + (rand() % 26));
  return s;
}

void 
SaveCritPairs(const std::string &filename, 
                   const std::vector<CriticalPair> &vCrit) {
  std::ofstream file;
  file = std::ofstream(WORKDIR + "/" + filename, 
                       std::ios::in 
                       | std::ios::trunc);
  if (!file.is_open()) 
    throw std::runtime_error("Unable to open " + WORKDIR + "/" + filename);

  file << vCrit.size() << std::endl;
  for (const auto& it : vCrit) {
    file << it.left().second.row()            << " ";
    file << it.left().second.pMatrix()->id()  << " ";
    file << it.right().second.row()           << " ";
    file << it.right().second.pMatrix()->id() << std::endl;
  }
  file.close();
}

void LoadCritPairs(const std::string& filename, 
                   std::vector<CriticalPair>& vCrit,
                   std::map<std::string, std::shared_ptr<StorageMatrix>>& UniqueMatIds) {
  vCrit.clear();

  std::ifstream File(WORKDIR + "/" + filename,
      std::ios::out);
  if (!File.is_open())
    throw std::runtime_error("Unable to open file " + WORKDIR + "/" + filename);
    
  std::size_t row1, row2;
  std::string id1, id2;
  std::size_t nb_crit_pairs;

  File >> nb_crit_pairs;

  for (std::size_t i = 0; i < nb_crit_pairs; ++i) {
    File >> row1 >> id1 >> row2 >> id2;
    auto iSearch1 = UniqueMatIds.find(id1);

    if (iSearch1 == UniqueMatIds.end()) {
      std::shared_ptr<RowMajMat> pMat;
      TINYGB_TRY_AND_THROW(ReadRowMajMat(&pMat, "matrices/" + id1 + ".mat"), 
                           "fn LoadCritPairs",
                           "Unable to read RowMajMat");

      std::pair<std::string, std::shared_ptr<StorageMatrix>>
        tmp_to_insert(id1, 
                      std::make_shared<StorageMatrix>(
                        pMat,
                        pMat->col_dec_,
                        pMat->row_dec_,
                        id1));

      UniqueMatIds.insert(tmp_to_insert);
    }

    auto iSearch2 = UniqueMatIds.find(id2);
    // bad duplicated code
    if (iSearch2 == UniqueMatIds.end()) 
    {
      std::shared_ptr<RowMajMat> pMat;
      TINYGB_TRY_AND_THROW(ReadRowMajMat(&pMat, "matrices/" + id2 + ".mat"), 
                           "fn LoadCritPairs",
                           "Unable to read RowMajMat");

      std::pair<std::string, std::shared_ptr<StorageMatrix>>
        tmp_to_insert(id2, 
                      std::make_shared<StorageMatrix>(
                        pMat,
                        pMat->col_dec_,
                        pMat->row_dec_,
                        id2));

      UniqueMatIds.insert(tmp_to_insert);
    }

    vCrit.push_back(CriticalPair(PolynomialInMatrix(UniqueMatIds[id1], row1),
                                 PolynomialInMatrix(UniqueMatIds[id2], row2)));

  }
  File.close();
}

void SaveReductors(const std::string& filename,
                   const std::vector<std::pair<Monomial, PolynomialInMatrix>>& vRed) {
  std::ofstream file;
  file = std::ofstream(WORKDIR + "/" + filename, 
                          std::ios::in 
                        | std::ios::trunc);

  if (!file.is_open()) 
    throw std::runtime_error("Unable to open " + WORKDIR + "/" + filename);

  file << vRed.size() << std::endl;
  for (const auto& it : vRed) {
    it.first.Print(file) << " ";
    file << it.second.row()            << " ";
    file << it.second.pMatrix()->id()  << std::endl;
  }
  file.close();
}
                   

void LoadReductors(const std::string& filename,
                   std::vector<std::pair<Monomial, PolynomialInMatrix>>& vRed,
                   std::map<std::string, std::shared_ptr<StorageMatrix>>& UniqueMatIds) {

  // TODO: useful?
  vRed.clear();

  std::ifstream File(WORKDIR + "/" + filename,
      std::ios::out);
  if (!File.is_open())
    throw std::runtime_error("Unable to open file " + WORKDIR + "/" + filename);
    
  Monomial m;
  std::size_t row;
  std::string id;
  std::size_t nb_reds;
  File >> nb_reds;

  for (std::size_t j = 0; j < nb_reds; ++j) {
    for (std::size_t i = 0; i < NB_VARS; ++i)
      File >> m(i);
    File >> row >> id;

    // Bad code duplication
    auto iSearch = UniqueMatIds.find(id);

    if (iSearch == UniqueMatIds.end()) {
      std::shared_ptr<RowMajMat> pMat;
      TINYGB_TRY_AND_THROW(ReadRowMajMat(&pMat, "matrices/" + id + ".mat"), 
                           "fn LoadReductors",
                           "Unable to load reductors");
      std::shared_ptr<RowMajMat> pMatshr(pMat);

      std::pair<std::string, std::shared_ptr<StorageMatrix>>
        tmp_to_insert(id, 
                      std::make_shared<StorageMatrix>(
                        pMatshr,
                        pMatshr->col_dec_,
                        pMatshr->row_dec_,
                        id));

      UniqueMatIds.insert(tmp_to_insert);
    }

    vRed.push_back(std::pair<Monomial, PolynomialInMatrix>{m, 
        PolynomialInMatrix{UniqueMatIds[id], row}});

  }
  File.close();
}

void SaveGB(const std::string& filename,
            const std::vector<PolynomialInMatrix>& gb) {
  std::ofstream file;
  file = std::ofstream(WORKDIR + "/" + filename, 
                          std::ios::in 
                        | std::ios::trunc);

  if (!file.is_open()) 
    throw std::runtime_error("Unable to open " + WORKDIR + "/" + filename);

  file << gb.size() << std::endl;
  for (const auto& it : gb) {
    file << it.row()     << " ";
    file << it.pMatrix()->id()  << std::endl;
  }
  file.close();
}
                   

void LoadGB(const std::string& filename,
            std::vector<PolynomialInMatrix>& gb,
            std::map<std::string, std::shared_ptr<StorageMatrix>>& UniqueMatIds) {

  // TODO: replace by returning a value, instead of passing a reference
  gb.clear();

  std::ifstream File(WORKDIR + "/" + filename,
                     std::ios::out);
  if (!File.is_open())
    throw std::runtime_error("Unable to open file " + WORKDIR + "/" + filename);
    
  Monomial m;
  std::size_t row;
  std::string id;
  std::size_t nb_reds;
  File >> nb_reds;

  for (std::size_t j = 0; j < nb_reds; ++j) {
    File >> row >> id;

    // Bad code duplication
    auto iSearch = UniqueMatIds.find(id);

    if (iSearch == UniqueMatIds.end()) {
      std::shared_ptr<RowMajMat> pMat;
      TINYGB_TRY_AND_THROW(ReadRowMajMat(&pMat, "matrices/" + id + ".mat"), 
                           "fn LoadGB",
                           "Unable to load GB");
      std::shared_ptr<RowMajMat> pMatshr(pMat);

      std::pair<std::string, std::shared_ptr<StorageMatrix>>
        tmp_to_insert(id, 
                      std::make_shared<StorageMatrix>(
                        pMatshr,
                        pMatshr->col_dec_,
                        pMatshr->row_dec_,
                        id));

      UniqueMatIds.insert(tmp_to_insert);
    }

    gb.push_back(PolynomialInMatrix{UniqueMatIds[id], row});
  }
  File.close();
}


bool HasPowerOfVariable(const std::set<Monomial>& S, unsigned i) {
  for (const auto& it : S) {
    bool flag = true;
    for (std::size_t j = 0; j < NB_VARS; ++j)
      if (j != i && it.getexp(j) != 0)
        flag = false;
    if (flag) return true;
  } 
  return false;
}

bool HasAllPowersOfVars(const std::set<Monomial>& S) {
  bool flag = true;
  for (unsigned i = 0; i < NB_VARS; ++i)
    if (!HasPowerOfVariable(S, i))
      flag = false;
  return flag;
}
  
bool IsMinimalGB(const std::vector<Polynomial> &gb) {
  for (auto i = gb.begin(); i != gb.end(); ++i)
    for (auto j = gb.begin(); j != gb.end(); ++j)
      if (i != j && i->LeadingMonomial().IsDivisibleBy(j->LeadingMonomial()))
        return false;
  return true;
}

void RemoveRedundantPolynomials(std::vector<PolynomialInMatrix> &gb,
                                const Monomial& m) {
  for (auto it = gb.begin(); it != gb.end(); ++it) {
    if (it->LeadingMonomial().IsDivisibleBy(m)) {
      it = gb.erase(it);
      --it;
    }
  }
}

}  // namespace tinygb
