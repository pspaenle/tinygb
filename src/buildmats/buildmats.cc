#define TINYGB_EXECUTABLE

#include "./glob_params.h"
#include "./common.h"
#include "linalg/matrix_factory.h"
#include "./macros.h"
#include "./logger.h"

namespace tinygb {

void buildmats(const std::string& critpairs_file,
               const std::string& stepreductors_file,
               const std::string& matA_file,
               const std::string& matB_file,
               const std::string& matC_file,
               const std::string& matD_file) {

  std::map<std::string, std::shared_ptr<StorageMatrix>> UniqueMatIds;
  std::vector<CriticalPair> step_critical_pairs;

  TINYGB_TRY(LoadCritPairs(critpairs_file, step_critical_pairs, UniqueMatIds), "buildmats");

  std::vector<std::pair<Monomial, PolynomialInMatrix>> step_reductors;

  TINYGB_TRY(LoadReductors(stepreductors_file, step_reductors, UniqueMatIds), "buildmats");

  std::map<Monomial, std::size_t> vvM;
  std::set<Monomial> lv1, lv2, tlv2;
  std::vector<Monomial> v1, v2;

  for (const auto& itPP : step_reductors) {
    lv1.insert((itPP.second.LeadingMonomial()) * itPP.first);
    itPP.second.AddMonomialsMultipliedByCofactor(tlv2, itPP.first);
  }

  for (const auto& itPP : step_critical_pairs) {
    itPP.left().second.AddMonomialsMultipliedByCofactor(tlv2, itPP.left().first);
    itPP.right().second.AddMonomialsMultipliedByCofactor(tlv2, itPP.right().first);
//    tlv2.insert(itPP.left().first*itPP.left().second.LeadingMonomial());
    // TODO(pj) : improve efficiency
  }

  std::set_difference(tlv2.begin(), tlv2.end(), lv1.begin(), lv1.end(),
      std::inserter(lv2, lv2.end()));


  for (const auto& it : lv1)
    v1.push_back(it);
  for (const auto& it : lv2)
    v2.push_back(it);

  lv1.clear();
  lv2.clear();
  tlv2.clear();

  std::sort(v1.begin(), v1.end());
  std::reverse(v1.begin(), v1.end());
  std::sort(v2.begin(), v2.end());
  std::reverse(v2.begin(), v2.end());

  std::shared_ptr<SpMat> A = MatrixFactory::BuildSparseUpperTriangular(step_reductors, v1);
  std::shared_ptr<RowMajMat> B = MatrixFactory::BuildRowMajor(step_reductors, v2);

  step_reductors.clear();

  std::vector<Polynomial> crit_pols;

  for (const auto& it : step_critical_pairs) {
    Polynomial tmp = it.left().second.to_polynomial()*it.left().first;
    tmp -= it.right().second.to_polynomial()*it.right().first;
    tmp.DivideByLeadingCoefficient();
    crit_pols.push_back(tmp);
  }

  std::shared_ptr<RowMajMat> C = 
    MatrixFactory::BuildRowMajor(crit_pols, v1);
  std::shared_ptr<RowMajMat> D = 
    MatrixFactory::BuildRowMajor(crit_pols, v2);

  step_critical_pairs.clear();

  // TODO(pj): Not good to give already row_decorations
  B->row_dec_ = v1;
  B->col_dec_ = v2;
  C->col_dec_ = v1;
  D->col_dec_ = v2;

  std::cerr << "buildmats: " << std::endl;
  std::cerr << "  F4 matrix size: (" << A->row_size() << "+" << C->row_size() <<
    ")*(" << A->column_size() << "+" << B->column_size() << ")" << std::endl;
  std::cerr << "  Total densities of A, B, C, D: " << 
    A->density()*100 << "%, " <<
    B->density()*100 << "%, " <<
    C->density()*100 << "%, " <<
    D->density()*100 << "%. " << std::endl;

  TINYGB_TRY(WriteSpMat    (*A, matA_file), "buildmats");
  TINYGB_TRY(WriteRowMajMat(*B, matB_file), "buildmats");
  TINYGB_TRY(WriteRowMajMat(*C, matC_file), "buildmats");
  TINYGB_TRY(WriteRowMajMat(*D, matD_file), "buildmats");
}

}  // tinygb

int main(int argc, char **argv) {
  tinygb::Logger("/tmp/log_tinygb").log("buildmats start");

  (void)argc; // To get rid of the warning -Wunused-parameter argc

  tinygb::WORKDIR = (std::string)argv[1];

  TINYGB_TRY(tinygb::load_glob_params(tinygb::WORKDIR + "/global_parameters.txt"), "buildmats");

  tinygb::buildmats(argv[2], argv[3], argv[4], argv[5], argv[6], argv[7]);
  tinygb::Logger("/tmp/log_tinygb").log("buildmats stop");
  return EXIT_SUCCESS;
}

