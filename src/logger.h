#ifndef LOGGER_H_
#define LOGGER_H_

#include <string>

namespace tinygb {

class Logger {
 public:
  Logger(const std::string filename)
    : filename_(filename) {}

  void log(const std::string& s);
  void erase();

 private:
  std::string filename_;
};

}  // namespace tinygb

#endif
