#define TINYGB_EXECUTABLE

#include <string>
#include "./common.h"
#include "./glob_params.h"
#include "./matrices/spmat.h"
#include "./matrices/rowmajmat.h"
#include "./finite_field_arithmetic/givaro_wrapper.h"
#include "./macros.h"
#include "./logger.h"

namespace tinygb {

void utsolve(std::string res_file, std::string matA_file, std::string matB_file) {
  std::shared_ptr<SpMat> pA = nullptr; 
  std::shared_ptr<RowMajMat> pB = nullptr;

  ReadSpMat(&pA, matA_file);
  ReadRowMajMat(&pB, matB_file);
  
  pB->MultiplyByInverse(*pA); 
  WriteRowMajMat(*pB, res_file);
}

}

int main(int argc, char **argv) {
  tinygb::Logger("/tmp/log_tinygb").log("utsolve   start");

  (void)argc; // To get rid of the warning -Wunused-parameter argc

  tinygb::WORKDIR = (std::string)argv[1];

  TINYGB_TRY(tinygb::load_glob_params(tinygb::WORKDIR + "/global_parameters.txt"), "utsolve");

  tinygb::utsolve(argv[2], argv[3], argv[4]);
  tinygb::Logger("/tmp/log_tinygb").log("utsolve   stop");
  return EXIT_SUCCESS;
}

