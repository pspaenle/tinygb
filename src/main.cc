#define TINYGB_EXECUTABLE

#include <fstream>
#include <unistd.h>
#include "./glob_params.h"
#include "./f4_launcher.h"
#include "./logger.h"

void usage() {
  std::cerr << "usage: tinygb WORKDIR INPUTSYS [OPTIONS]" << std::endl;
  std::cerr << "  reads a polynomial system over a finite field in the file \
    INPUTSYS" << std::endl;
  std::cerr << "  and writes the grevlex Gröbner basis on the standard " <<
    "output" << std::endl;
  std::cerr << std::endl;
  std::cerr << "OPTIONS:" << std::endl;
  std::cerr << "  -o outputfile" << std::endl;
  std::cerr << "     write the Gröbner basis in a file instead of the " <<
    "standard output" << std::endl;
  std::cerr << "  -s maxstep" << std::endl;
  std::cerr << "     stops the computation after a given number of steps" << std::endl;
  std::cerr << "  -c" << std::endl;
  std::cerr << "     stops the computation as soon the leading monomials ";
  std::cerr << "of the current basis generate a 0-dim ideal" << std::endl;
  std::cerr << "  -h" << std::endl;
  std::cerr << "     print usage" << std::endl;
//  std::cerr << "  -v" << std::endl;
//  std::cerr << "     set verbose mode on" << std::endl;
}

int main(int argc, char** argv) {
  tinygb::Logger("/tmp/log_tinygb").erase();
  int c;
  int flag_output_file = 0;
  std::ostream* output  = &std::cout;
  std::string sys_file;
  
  while ((c = getopt(argc, argv, "chvo:s:")) != -1)
    switch (c) {
      case 'c':
        tinygb::STOP_CRITERION = true;
        break;
      case 'h':
        usage();
        return 0;
        break;
//      case 'v':
//        tinygb::GLOBAL_LOG_LEVEL = tinygb::LOG_INFO;
//        break;
      case 'i':
        sys_file = std::string(optarg);
        break;
      case 'o':
        output = new std::ofstream(optarg);
        flag_output_file = 1;
        break;
      case 's':
        tinygb::MAXSTEP_FLAG = true;
        tinygb::MAXSTEP      = atoi(optarg);
        break;
      default:
        std::cerr << "option not recognized, aborting." << std::endl;
        abort();
    }

  if (optind >= argc) {
    usage();
    exit(EXIT_FAILURE);
  }

  tinygb::initF4(argv[optind], argv[optind+1], *output);

  if (flag_output_file)
     delete output;

  exit(EXIT_SUCCESS);
}


