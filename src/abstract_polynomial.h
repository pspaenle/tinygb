#ifndef __ABSTRACT_POLYNOMIAL_H__
#define __ABSTRACT_POLYNOMIAL_H__

#include <type_traits>
#include "./abstract_field.h"

namespace tinygb {

// AbstractPolynomial is the global API for multivariate polynomials. Concrete
// implementations of polynomials are implemented in subclasses via static
// polymorphism.

template <class Field, class T> // T is the CRTP parameter
class AbstractPolynomial {
  static_assert(std::is_base_of<AbstractField<Field>, Field>::value, 
                "Field should be derived from AbstractField"); 
  public:
  Monomial LeadingMonomial() const {
    return static_cast<T*>(this)->LeadingMonomial();}
  typename Field::Element LeadingCoefficient() const {
    return static_cast<T*>(this)->LeadingCoefficient();}
  void DivideByLeadingCoefficient() {
    static_cast<T*>(this)->DivideByLeadingCoefficient();}
};

}  // namespace tinygb
#endif

