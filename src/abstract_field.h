#ifndef __FIELD_H__
#define __FIELD_H__

#include <type_traits>

namespace tinygb {

// AbstractField and AbstractFieldElement are the API for fields and their
// elements. Concrete implementations of fields are implemented in subclasses
// via static polymorphism.

template <typename T> // CRTP
class AbstractField {
  public:
  std::string description() const {
    return static_cast<T*>(this)->description();
  }

  template <typename U> // CRTP
  class Element {
    public:
    typedef T Parent;

    void operator+=(const Element<U>& e) {
      static_cast<T*>(this)->operator+=(e);
    }
    void operator-=(const Element<U>& e) {
      static_cast<T*>(this)->operator-=(e);
    }
    void operator*=(const Element<U>& e) {
      static_cast<T*>(this)->operator*=(e);
    }
    void operator/=(const Element<U>& e) {
      static_cast<T*>(this)->operator/=(e);
    }
    void operator==(const Element<U>& e) {
      static_cast<T*>(this)->operator==(e);
    }
    void operator!=(const Element<U>& e) {
      static_cast<T*>(this)->operator!=(e);
    }
    void neg() {
      static_cast<T*>(this)->neg();
    }
    void inv() {
      static_cast<T*>(this)->inv();
    }
    bool IsZero() const {
      return static_cast<T*>(this)->isZero();
    }
  };
};

}  // namespace tinygb
#endif

