// Generic macro for terminal try/catch blocks

#define TINYGB_TRY(FN_CALL, PREFIX)                                           \
  try{ FN_CALL; } catch (const std::exception& e) {                           \
    std::cout << std::string(PREFIX) + ":"  << e.what() << std::endl;                         \
  }                                                                          

// Try/catch exception, then forward it to the calling function with the 
// adapted message EXCEPTION

#define TINYGB_TRY_AND_THROW(FN_CALL, PREFIX, EXCEPTION_MESG)                 \
  try{ FN_CALL; } catch (const std::exception& e) {                           \
    std::cout << std::string(PREFIX) + ":" << e.what() << std::endl;                         \
    throw std::runtime_error(EXCEPTION_MESG);                               \
  }                                                                          
