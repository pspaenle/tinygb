#include "./monomial.h"

namespace tinygb {

// static member definition
Monomial_pool Monomial::m_pool;

// Acknowledgements to Jeremie Detrey for this SSE/AVX code
// AVX code disabled
// TODO(pj): enable AVX
//#ifdef __MONOMIAL_ORDERING
//#  if __MONOMIAL_ORDERING == GREVLEX
//#    if NB_VARSMAX <= 16 && SIZE_EXPONENT <= 7
//#      include <x86intrin.h>
//
//int monomial_lt_avx(uint8_t *_m1, uint8_t *_m2,
//                    uint8_t *_mask) {
//  __m128i mask = *reinterpret_cast<__m128i*>(_mask);
//  __m128i m1   = _mm_and_si128(*reinterpret_cast<__m128i*>(_m1), mask);
//  __m128i m2   = _mm_and_si128(*reinterpret_cast<__m128i*>(_m2), mask);
//  __m128i t1, t2;
//
//  t1 = _mm_add_epi16(_mm_unpacklo_epi8(m1, _mm_setzero_si128()),
//                     _mm_unpackhi_epi8(m1, _mm_setzero_si128()));
//  t2 = _mm_add_epi16(_mm_unpacklo_epi8(m2, _mm_setzero_si128()),
//                     _mm_unpackhi_epi8(m2, _mm_setzero_si128()));
//  t1 = _mm_hadd_epi16(t1, t2);
//  t1 = _mm_hadd_epi16(t1, _mm_setzero_si128());
//  t1 = _mm_hadd_epi16(t1, _mm_setzero_si128());
//
//  uint16_t d1 = _mm_extract_epi16(t1, 0);
//  uint16_t d2 = _mm_extract_epi16(t1, 1);
//
//  if (d1 < d2)
//    return 1;
//  if (d1 > d2)
//    return 0;
//
//  t1 = _mm_cmpgt_epi8(m1, m2);
//  t2 = _mm_cmpgt_epi8(m2, m1);
//
//  return _mm_movemask_epi8(t1) > _mm_movemask_epi8(t2);
//}
//
//bool Monomial::operator<(const Monomial &m2) const {
//  uint8_t mask[16] = { 0 };
//  for (unsigned i = 0; i < NB_VARS; ++i)
//    mask[i]  = 0xff;
//  return static_cast<bool>(monomial_lt_avx(exp, m2.exp, mask));
//}
//
//#    else
bool Monomial::operator<(const Monomial &m2) const {
  expo_int d1 = Degree();
  expo_int d2 = m2.Degree();
  if (d1 < d2) return true;
  if (d1 > d2) return false;
  int i = NB_VARS-1;
  for (; i > 0; --i)
    if (getexp(i) > m2.getexp(i))
      return true;
    else if (getexp(i) < m2.getexp(i))
      return false;
  return false;
}
//#    endif
//#  elif __MONOMIAL_ORDERING == LEX
//bool Monomial::operator<(const Monomial &m2) const {
//  for (unsigned i = 0; i < NB_VARS; ++i)
//    if (getexp(i) < m2.getexp(i))
//      return true;
//    else if (getexp(i) > m2.getexp(i))
//      return false;
//  return false;
//}
//#  else
//#  error A Monomial ordering should be declared
//bool Monomial::operator<(const Monomial &m2) const {
//  log(LOG_ERROR) << "error: Monomial ordering not defined" << std::endl;
//  exit(0);
//}
//#  endif
//#endif

Monomial Monomial::LCM(const Monomial& m1, const Monomial &m2) {
  Monomial R;
  for (std::size_t i = 0; i < NB_VARS; i++)
    if (m2.getexp(i) > m1.getexp(i))
      R.setexp(i, m2.getexp(i));
    else
      R.setexp(i, m1.getexp(i));
  return R;
}

// Computes the cofactors LCM(m1,m2)/m1 and LCM(m1,m2)/m2.
std::pair<Monomial, Monomial> Monomial::Cofactors(const Monomial &m2) const {
  Monomial R1, R2;
  for (unsigned i = 0; i < NB_VARS; i++)
    if (getexp(i) > m2.getexp(i))
      R2.setexp(i, getexp(i) - m2.getexp(i));
    else
      R1.setexp(i, m2.getexp(i) - getexp(i));
  return std::pair<Monomial, Monomial>(R1, R2);
}

bool Monomial::IsDivisibleBy(const Monomial &m2) const {
  for (unsigned i = 0; i < NB_VARS; ++i)
    if (getexp(i) < m2.getexp(i))
      return false;
  return true;
}

std::ostream& Monomial::Print(std::ostream& o) const {
  for (unsigned i = 0; i < NB_VARS; ++i) {
    o << (unsigned)getexp(i);
    if (i != NB_VARS-1)
      o << " ";
  }
  return o;
}

std::ostream& operator<<(std::ostream& o, const Monomial &m) {
  o << "1";
  for (unsigned i = 0; i < NB_VARS; ++i)
    if (m.getexp(i)) o << "*x" << i << "^" << (unsigned)m.getexp(i);
  return o;
}


}  // namespace tinygb
