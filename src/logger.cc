#include "logger.h"

namespace tinygb {

void Logger::log(const std::string& s) {
  std::system(("echo $(date +\"%s\") \"" + s + "\" >> " + filename_).c_str());
}

void Logger::erase() {
  std::system(("rm -f " + filename_).c_str());
}

}  // namespace tinygb

