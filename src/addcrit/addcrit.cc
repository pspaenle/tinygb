#define TINYGB_EXECUTABLE

#include "./glob_params.h"
#include "./critical_pair.h"
#include "matrices/rowmajmat.h"
#include "./macros.h"
#include "./logger.h"

namespace tinygb {

// Gebauer/Moller installation of Buchberger's criteria (see description
// in Vanessa Vitse's thesis).
// Inserts a polynomial f in the Gröbner basis G and updates the list of
// critical pairs using Gebauer/Moller algorithm.
// Warning: this version is a duplicate of the code in f4_toplevel.h, remove
// when this has been incorporated in the main code.

void InsertPolynomialGebauerMoller(std::vector<CriticalPair> &global_critical_pairs,
                                   std::vector<PolynomialInMatrix> &gb,
                                   const PolynomialInMatrix &f) {
  for (auto it = global_critical_pairs.begin(); it != global_critical_pairs.end(); ++it) {
    // TODO(pj): unclear, simplify.
    if (it->LCM() != Monomial::LCM(it->left().second.LeadingMonomial(), f.LeadingMonomial()) &&
        it->LCM() != Monomial::LCM(it->right().second.LeadingMonomial(), f.LeadingMonomial()) &&
        it->LCM().IsDivisibleBy(Monomial::LCM(it->left().second.LeadingMonomial(), f.LeadingMonomial())) &&
        it->LCM().IsDivisibleBy(Monomial::LCM(it->right().second.LeadingMonomial(), f.LeadingMonomial()))) {
      it = global_critical_pairs.erase(it);
      it--;
    }
  }
  std::vector<CriticalPair> S0, S1, S2;
  for (const auto& g : gb) {
    if (Monomial::LCM(g.LeadingMonomial(), f.LeadingMonomial()) == 
        g.LeadingMonomial() * f.LeadingMonomial())
      S0.push_back(CriticalPair(f, g));
    else
      S1.push_back(CriticalPair(f, g));
  }
  while (!S1.empty()) {
    // TODO(pj): why selecting last critpair in S1?
    CriticalPair cp1 = S1[S1.size() - 1];
    S1.erase(S1.end()-1);
    bool flag = false;
    Monomial tmp_lcm = cp1.LCM();
    for (const auto& current_critical_pair : S0) {
      if (tmp_lcm.IsDivisibleBy(current_critical_pair.LCM())) {
        flag = true;
        break;
      }
    }
    if (!flag) {
      for (const auto& current_critical_pair : S1) {
        if (tmp_lcm.IsDivisibleBy(current_critical_pair.LCM())) {
          flag = true;
          break;
        }
      }
    }
    if (!flag) {
      for (const auto& current_critical_pair : S2) {
        if (tmp_lcm.IsDivisibleBy(current_critical_pair.LCM())) {
          flag = true;
          break;
        }
      }
    }
    if (!flag) {
      S2.push_back(cp1);
      global_critical_pairs.push_back(cp1);
    }
  }
  if (f.IsTopReducible(gb))
    return;
  RemoveRedundantPolynomials(gb, f.LeadingMonomial());
  gb.push_back(f);
}

void addcrit(const std::string& mat_newpols_file,
             const std::string& critpair_file,
             const std::string& gb_file) {

  std::map<std::string, std::shared_ptr<StorageMatrix>> UniqueMatIds;
  std::vector<CriticalPair> VecCrit;
  std::vector<PolynomialInMatrix> gb;

  TINYGB_TRY(LoadGB(gb_file, gb, UniqueMatIds), "addcrit");
  TINYGB_TRY(LoadCritPairs(critpair_file, VecCrit, UniqueMatIds), "addcrit");
  
  if (!UniqueMatIds.count(mat_newpols_file)) {
    std::shared_ptr<RowMajMat> pMat;
    TINYGB_TRY(ReadRowMajMat(&pMat, "matrices/" + mat_newpols_file + ".mat"), "addcrit");
    std::pair<std::string, std::shared_ptr<StorageMatrix>>
      tmp_to_insert(mat_newpols_file,
                    std::make_shared<StorageMatrix>(pMat,
                                                    pMat->col_dec_,
                                                    pMat->row_dec_,
                                                    mat_newpols_file));

    UniqueMatIds.insert(tmp_to_insert);
  }

  for (std::size_t i = 0; i < UniqueMatIds[mat_newpols_file]->row_size(); ++i)
    InsertPolynomialGebauerMoller(VecCrit, 
                                  gb, 
                                  PolynomialInMatrix{UniqueMatIds[mat_newpols_file], i});
  TINYGB_TRY(SaveGB(gb_file, gb), "addcrit");
  TINYGB_TRY(SaveCritPairs(critpair_file, VecCrit), "addcrit");
}

}  // namespace tinygb


int
main(int argc, char **argv) {
  tinygb::Logger("/tmp/log_tinygb").log("addcrit   start");

  (void)argc; // To get rid of the warning -Wunused-parameter argc
  tinygb::WORKDIR = (std::string)argv[1];

  TINYGB_TRY(tinygb::load_glob_params(tinygb::WORKDIR + "/global_parameters.txt"), "addcrit");
  
  tinygb::addcrit(argv[2], argv[3], argv[4]);
  tinygb::Logger("/tmp/log_tinygb").log("addcrit   stop");
  return EXIT_SUCCESS;
}


