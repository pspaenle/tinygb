#include "./critical_pair.h"

namespace tinygb {

CriticalPair::CriticalPair(const PolynomialInMatrix &p1,
                           const PolynomialInMatrix &p2) {
  std::pair<Monomial, Monomial> c =
    p1.LeadingMonomial().Cofactors(p2.LeadingMonomial());
  left_ = std::pair<Monomial, PolynomialInMatrix>(c.first, p1);
  right_ = std::pair<Monomial, PolynomialInMatrix>(c.second, p2);
  LCM_ = left_.first*left_.second.LeadingMonomial();
}

// TODO(pj): extends to different strategies.
// TODO(pj): where is this used?
bool CriticalPair::operator<(CriticalPair &cp2) {
  if (Degree() < cp2.Degree()) return true;
  return false;
}

std::ostream& operator<<(std::ostream& o,
                         const std::pair<Monomial, PolynomialInMatrix> P) {
  o << "(" << P.first << "," << P.second << ")";
  return o;
}

std::ostream& operator<<(std::ostream& o, const CriticalPair& cp) {
  o << cp.left_ << " " << cp.right_;
  return o;
}

// TODO(pj): improve by merging both lists
std::list<Monomial> CriticalPair::GetMonomials() const {
  std::list<Monomial> result;
  for (const auto& m : left_.second.GetMonomials())
    result.push_back(left_.first * m);
  for (const auto& m : right_.second.GetMonomials())
    result.push_back(right_.first * m);
  return result;
}

Polynomial CriticalPair::Spolynomial() const {
  Polynomial tmp = left_.second.to_polynomial();
  tmp = tmp * left_.first;
  Polynomial R = right_.second.to_polynomial();
  R  = R * right_.first;
  R -= tmp;
  R.Normalize();
  return R;
}

}  // namespace tinygb
