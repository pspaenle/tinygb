#ifndef __POLYNOMIAL_H__
#define __POLYNOMIAL_H__

#include <list>
#include <utility>
#include "./common.h"
#include "./monomial.h"
#include "./abstract_polynomial.h"
#include "finite_field_arithmetic/givaro_wrapper.h"

namespace tinygb {

class Polynomial : public AbstractPolynomial<GFp_Givaro, Polynomial> {
 public:
  typedef std::pair<Monomial, GivaroWrapper> Term;

  Polynomial() {
      terms_ = std::list<Term>();
    }
  explicit Polynomial(const std::list<Term>& terms_)
    : terms_(terms_) {}

  const std::list<Term>& terms() const {
    return terms_;
  }

  // Put the list of terms in standard form. Depends on the monomial ordering.
  // The list is ordered from smallest to largest
  // TODO(pj): leading monomial should be first
  void Normalize();

  void SetToZero() {
    terms_.clear();
  }

  void insert(Monomial m, GivaroWrapper e) {
    terms_.push_back(std::pair<Monomial, GivaroWrapper>(m, e));
  }

  // Assumes that the polynomial's list of terms is normalized
  const Monomial& LeadingMonomial() const {
    assert(!terms_.back().second.is_zero());
    return terms_.back().first;
  }

  const GivaroWrapper& LeadingCoefficient() const {
    return terms_.back().second;
  }

  void DivideByLeadingCoefficient();

  unsigned Degree() const {
    // TODO(pj): Extend for non-degree orderings.
    return LeadingMonomial().Degree();
  }

  // Reduces leading monomial by the polynomial given.
  // Returns true if leading monomial is divisible by the leading monomial of
  // the argument. When this function returns false, then nothing has been
  // modified.
  // Assumes that terms_ is normalized.
  bool TopReductionBy(const Polynomial &);

  // Returns true if the leading monomial is divisible by the leading monomial
  // of the argument.
  // Assumes that terms_ is normalized.
  inline bool IsTopReducibleBy(const Polynomial &) const;

  Polynomial operator*(const Monomial&) const;
  void operator+=(const Polynomial&);
  void operator-=(const Polynomial&);
  void operator*=(const GivaroWrapper&);
  void operator/=(const GivaroWrapper&);
  void Print(std::ostream& out) const;
  inline bool operator<(const Polynomial& P) const;

  friend std::ostream& operator<<(std::ostream&, const Polynomial&);

 private:
  // Adds coefficients of consecutive terms if they share the same monomials.
  // Useful for adding polynomials. Do not use if terms_ is not sorted.
  void CombineMonomials();
  bool IsEmpty() const {
    return terms_.empty();
  }
  // Removes terms with coefficient 0
  void RemoveZeroTerms();

  std::list<Term> terms_;
};

inline bool Polynomial::IsTopReducibleBy(const Polynomial &g) const {
  assert(!g.IsEmpty());
  if (IsEmpty()) return false;
  return LeadingMonomial().IsDivisibleBy(g.LeadingMonomial());
}

inline bool Polynomial::operator<(const Polynomial& P) const {
  return LeadingMonomial() < P.LeadingMonomial();
}

}  // namespace tinygb

#endif  // POLYNOMIAL_H_
