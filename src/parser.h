#ifndef __PARSER_H__
#define __PARSER_H__

#include <fstream>
#include <string>
#include <vector>
#include "./common.h"
#include "./polynomial.h"

// TODO(pj): check format (and raise error if incorrect, for instance, wrong
// number of exponent vectors)

namespace tinygb {

class Parser {
  std::istream& in_stream;

 public:
  explicit Parser(std::istream& _s): in_stream(_s) {}
  std::vector<Polynomial> parse() const;
};

std::vector<Polynomial> Parser::parse() const {
  std::vector<Polynomial> res;
  std::string s;
  unsigned c;
  expo_int *exp = new expo_int[NB_VARS];
  while (in_stream >> s) {
    Polynomial p;
    while (s != ";") {
      GivaroWrapper e(s);
      for (std::size_t i = 0; i < NB_VARS; ++i) {
        in_stream >> s;
        c = (expo_int)atoi(s.c_str());
        exp[i] = c;
      }
      Monomial m(exp);
      p.insert(m, e);
      in_stream >> s;
    }
    p.Normalize();
    res.push_back(p);
  }
  delete[] exp;
  return res;
}
}  // namespace tinygb

#endif  // PARSER_H_
