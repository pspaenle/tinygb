#define TINYGB_EXECUTABLE

#include "./glob_params.h"
#include "./matrices/rowmajmat.h"
#include "./finite_field_arithmetic/givaro_wrapper.h"
#include "./common.h"
#include "./macros.h"
#include "./logger.h"

namespace tinygb {

// return D - C*B

void 
matmulsub(
    std::string res_file,
    std::string matB_file, 
    std::string matC_file, 
    std::string matD_file) {

  std::shared_ptr<RowMajMat> pB = nullptr, pC = nullptr, pD = nullptr;
  ReadRowMajMat(&pB, matB_file);
  ReadRowMajMat(&pC, matC_file);
  ReadRowMajMat(&pD, matD_file);

  FFLAS::fgemm(GFp_Givaro::k,
      FFLAS::FflasNoTrans,
      FFLAS::FflasNoTrans,
      pD->row_size(),
      pB->column_size(),
      pB->row_size(),
      GFp_Givaro::k.mOne,
      pC->entries_, pB->row_size(),
      pB->entries_, pB->column_size(),
      GFp_Givaro::k.one,
      pD->entries_, pD->column_size());

  WriteRowMajMat(*pD, res_file);
}

}  // namespace tinygb

int main(int argc, char **argv) {
  tinygb::Logger("/tmp/log_tinygb").log("matmulsub start");

  (void)argc; // To get rid of the warning -Wunused-parameter argc

  tinygb::WORKDIR = (std::string)argv[1];

  TINYGB_TRY(tinygb::load_glob_params(tinygb::WORKDIR + "/global_parameters.txt"), "matmulsub");

  tinygb::matmulsub(argv[2], argv[3], argv[4], argv[5]);
  tinygb::Logger("/tmp/log_tinygb").log("matmulsub stop");
  return EXIT_SUCCESS;
}

