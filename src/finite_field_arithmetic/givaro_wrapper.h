#ifndef __GIVARO_WRAPPER_H__
#define __GIVARO_WRAPPER_H__

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include <givaro/modular-integer.h>
#pragma GCC diagnostic pop

#include "./abstract_field.h"

namespace tinygb {

class GFp_Givaro : public AbstractField<GFp_Givaro> {
  public:
    typedef Givaro::Integer elt_t;
    typedef Givaro::Modular<Givaro::Integer> ring_t;

    std::string description() const {
      return "Finite field of prime cardinality p";}

    static ring_t k;

    class Element : public AbstractField<GFp_Givaro>::Element<GFp_Givaro::Element> {
      public:
        elt_t v;

        // Constructors
        Element() {}
        Element(const char* n) { k.init(v, n); }
        Element(int n) { k.init(v, n); }
        Element(const mpz_class& n) { k.init(v, n);}
        Element(const elt_t& n) { k.init(v, n); }
        Element(const std::string& s) { k.init(v, s.c_str()); }

        // Copy constructor
        Element(const Element& x) { k.assign(v, x.v); }

        // TODO rule of 0/3/5
        Element& operator=(const Element& x) {
          k.assign(v, x.v);   
          return *this;
        }

        inline bool is_zero() const { return k.isZero(v); }
        friend std::ostream& operator<<(std::ostream&, const Element&);
        friend std::istream& operator>>(std::istream&, Element&);
        void operator+=(const Element& a) { k.addin(v, a.v); }
        void operator-=(const Element& a) { k.subin(v, a.v); }
        void operator*=(const Element& a) { k.mulin(v, a.v); }
        void operator/=(const Element& a) { k.divin(v, a.v); }
        bool operator==(const Element& a) const { return v == a.v; }
        bool operator!=(const Element& a) const { return v != a.v; }
        void neg() { v = -v; }
        void inv() { k.invin(v); }
        mpz_srcptr get_mpz() const { return v.get_mpz(); }
        void set_mpz(const mpz_class& a) { k.init(v, elt_t(a)); }
    };
};

typedef GFp_Givaro::Element GivaroWrapper;

std::ostream& operator<<(std::ostream&, const GivaroWrapper &);
std::istream& operator>>(std::istream&, GivaroWrapper&);

}  // namespace tinygb
#endif

