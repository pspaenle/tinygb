#include "./givaro_wrapper.h"

namespace tinygb {

GFp_Givaro::ring_t GFp_Givaro::k;

std::ostream& operator<<(std::ostream& o, const GivaroWrapper &e) {
  return GFp_Givaro::k.write(o, e.v);
}

std::istream& operator>>(std::istream& i, GivaroWrapper& x) {
  return (i >> x.v);
}

}
