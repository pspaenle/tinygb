#ifndef __CRITICAL_PAIR_H__
#define __CRITICAL_PAIR_H__

#include <set>
#include <utility>
#include "./monomial.h"
#include "./polynomial_in_matrix.h"
#include "./polynomial.h"

namespace tinygb {

// This class represents critical pairs, i.e. pairs ((m1,p1),(m2,p2)) such that
// the leading monomial of m1*p1 equals that of m2*p2.
// Polynomials p1 and p2 are assumed to be monic.

class CriticalPair {
 public:
  CriticalPair(const PolynomialInMatrix& p1,
               const PolynomialInMatrix& p2);

  const std::pair<Monomial, PolynomialInMatrix>& left() const {
    return left_;
  }

  const std::pair<Monomial, PolynomialInMatrix>& right() const {
    return right_;
  }

  Monomial LCM() const {
    return LCM_;
  }

  unsigned Degree() const {
    return left_.second.Degree()+left_.first.Degree();}

  Polynomial Spolynomial() const;

  // Returns the set of monomials that airse with non zero coeff in both sides. 
  // Unicity in the returned list is not ensured.
  std::list<Monomial> GetMonomials() const;

  bool operator<(CriticalPair &cp2);
  bool operator==(const CriticalPair &cp2) const;
  friend std::ostream& operator<<(std::ostream&, const CriticalPair&);

 private:
  std::pair<Monomial, PolynomialInMatrix> left_;
  std::pair<Monomial, PolynomialInMatrix> right_;
  Monomial LCM_;
};

}  // namespace tinygb

#endif  // CRITICAL_PAIR_H_
