#!/bin/bash

# This interrupts the script with CTRL-C
trap '
  trap - INT # restore default INT handler
  kill -s INT "$$"
' INT

TINYGB_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $TINYGB_PATH
source param.sh

echo -n "Compiling... "
./compile.sh
rc=$?
if [ "$rc" -ne 0 ]; then 
  echo "error."
  exit $rc
else
  echo "done."
fi
    
NC='\033[0m' # No Color

for f in $(ls --color=never tests/*.sys)
do
    printf "%-40s" $(basename $f)
    { ./compute_gb.sh $f 2>/dev/null | \
        diff - tests/$(basename -s .sys $f).gb ; rc=$?; }
    echo -n " ---- ";
    if [ $rc -eq 0 ] 
    then
        echo -e "\033[42;30;1m Pass ${NC}"
    else
        echo -e "\033[41;30;1m Fail ${NC}"
    fi
done
