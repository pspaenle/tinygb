#!/bin/bash

for step in selcrit selred buildmats utsolve matmulsub \
            rowech addcrit
do
    step_time=$(grep $step /tmp/log_tinygb | \
        awk 'NR % 2 == 0 {s+=$1} NR % 2 == 1 {s-=$1} END {print s}')
    printf "%-9s %8ds\n" $step $step_time
done
