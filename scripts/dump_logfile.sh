#!/bin/bash
# Process information in the log file to get the duration of subroutines.
# This script depends highly on the data structure of the log file (which may
# change)

#!/bin/bash
input="/path/to/txt/file"
while IFS= read -r line1
do
  if [ "$(echo $line1 | cut -d' ' -f2)" == "step" ]
  then
    echo
    echo $line1 | cut -d' ' -f2,3
    continue
  fi
  IFS= read -r line2
  progname=$(echo "$line1" | cut -d' ' -f2)
  start_time=$(echo "$line1" | cut -d' ' -f1)
  stop_time=$(echo "$line2" | cut -d' ' -f1)
  progtime=$(date -u -d "0 $stop_time sec - $start_time sec" +"%T")

  printf -v pad %10s
  padded_progname="$progname$pad"
  padded_progname="${padded_progname:0:10}"
  echo "$padded_progname $progtime"
done < "$1"
