#!/bin/bash

# This script is strongly inspired by the script provide-wdir.sh written by
# Emmanuel Thomé for the cado-nfs project.

# This script does the following: it creates a working directory, and it takes
# as input a command line which is executed after replacing every occurence of
# WORKING_DIR by the created directory. Upon exit, the working directory is
# cleaned, except if TINYGB_DEBUG is set to a nonzero value.

: ${TMPDIR:=/tmp}
wd=`mktemp -d $TMPDIR/tinygb.XXXXXXXXXXXXXX`

# gets script directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
TINYGB_PATH=$(dirname $DIR)

source $TINYGB_PATH/param.sh

cleanup() { rm -rf "$wd" ; }

if [ "$TINYGB_PERSISTENT_WORKDIR" -eq 1 ] ;
then
    echo "persistent workdir mode, data will be left in $wd"
else
    trap cleanup EXIT
fi

main=()

while [ $# -gt 0 ]
do
  if [ "$1" = "WORKING_DIR" ] ;
  then
    main=("${main[@]}" "$wd")
  else
    main=("${main[@]}" "$1")
  fi
  shift
done

if [ -f /usr/bin/time ]
then
    /usr/bin/time -f "Real time: %E" -o /dev/fd/2 "${main[@]}"
else
    "${main[@]}"
fi
echo -n "Memory usage: " 1>&2
du -sh $wd 1>&2

rc=$?

if [ "$TINYGB_PERSISTENT_WORKDIR" -eq 1 ] ;
then
    echo "persistent workdir mode, data will be left in $wd"
fi

exit $rc
