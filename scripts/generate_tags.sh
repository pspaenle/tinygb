#!/bin/bash

# gets script directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
TINYGB_PATH=$(dirname $DIR)

cd $TINYGB_PATH
ctags $(find src/ -name '*.h' -printf "%p " -o -name '*.cc' -printf "%p ")
