#!/usr/bin/perl

#
# This script converts a human readable polynomial system to the tinygb format
# It reads on the standard input.  First line is the number of variable, second
# line is the field cardinality.  Next are polynomials, as sums of terms of the
# form coeff*x1^exp1*x2^exp2*...  
# A line break must follow every polynomial
# Inert line breaks can occur after the symbol '+'
# White spaces can be inserted within polynomials.

use strict;
use warnings;

my $nbvar = <STDIN>;
my $field_card = <STDIN>;
chomp($nbvar);
chomp($field_card);
print "$nbvar\n";
print "$field_card\n";

while (<STDIN>) {
  chomp();
  my $line = $_;
  $line =~ tr/ //ds;
  my $flag_continue = 0;
  if (substr($line, -1) eq "+") {
    $flag_continue = 1;
  }
  my @terms = split('\+', $line);
  for my $term (@terms) {
    my @vec_exponents = ("0") x $nbvar;
    my @term_tokens = split('\*', $term);
    if (index($term_tokens[0], 'x') != -1) {
      print "1 "
    } else {
      print "$term_tokens[0] ";
      shift @term_tokens;
    }
    my $cur_var = 0;
    my $cur_exp = 0;
    for my $term_token (@term_tokens) {
      if (index($term_token, '^') != -1) {
        my @var_exp = split('\^', $term_token);
        substr $var_exp[0], 0, 1, "";
	$cur_var = $var_exp[0];
	$cur_exp = $var_exp[1];
      } else {
        $cur_var = $term_token;
        substr $cur_var, 0, 1, "";
	$cur_exp = "1";
      }
      $vec_exponents[$cur_var-1] += $cur_exp;
    }
    for (my $i=0; $i<$nbvar-1; $i++) {
      print "$vec_exponents[$i] ";
    }
    print "$vec_exponents[$nbvar-1]\n";
  }
  if (!$flag_continue) {
    print ";\n"
  }
}
