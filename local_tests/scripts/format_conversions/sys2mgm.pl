#!/usr/bin/perl

use strict;
use warnings;

my $nbvar = <STDIN>;
my $field_card = <STDIN>;

print "K := GF($field_card);\n";
print "R<";
for my $i (1 .. ($nbvar - 1)) { 
	print "x$i,";
}
print "x$nbvar> := PolynomialRing(K, $nbvar, \"grevlex\");\n";

print "sys := [\n";
my $flag_first = 1;

while(<STDIN>) {
  chomp();
  my $line = $_;
  if ("$line" eq ";") {
    print ",\n";
    $flag_first = 1;
  }
  else {
    my @mon_seq = split(' ', $line);
    if ($flag_first == 0) {
      print " +\n";
    }
    $flag_first = 0;
    print "$mon_seq[0]*";
    for my $i (1 .. ($nbvar - 1)) {
      print "x$i^$mon_seq[$i]*";
    }
    print "x$nbvar^$mon_seq[$nbvar]";
  }
}

print "0];\n";
