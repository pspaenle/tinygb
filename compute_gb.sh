#!/bin/bash

# This interrupts the script with CTRL-C
trap '
  trap - INT # restore default INT handler
  kill -s INT "$$"
' INT

# gets script directory
TINYGB_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $TINYGB_PATH
./compile.sh

# LD_LIBRARY_PATH should be replaced to avoid using a previously installed
# version of openblas. All required shared libraries should be in the tinygb
# directory.

if [ $? -eq 0 ] ;
then
    OLD_LD_PATH=$LD_LIBRARY_PATH
    export LD_LIBRARY_PATH="$TINYGB_PATH/lib";
    scripts/provide_wdir.sh build/tinygb WORKING_DIR $1
    export LD_LIBRARY_PATH=$OLD_LD_PATH
    $TINYGB_PATH/scripts/time_report.sh 1>&2
else
  echo "Compilation failed."
fi
